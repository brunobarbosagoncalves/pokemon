import { all, fork } from "redux-saga/effects"

//Import all sagas
import counter from "./counter.saga"
import traductor from "./i18n.saga"
import modal from "./modal.saga"
import feedback from './feedback.saga'


//export default
export default function *() {
  yield all([
    ...counter, 
    ...traductor, 
    ...modal,
    ...feedback
    ])
}
