import { call, put, select, all, takeEvery, delay } from 'redux-saga/effects';
import { toast } from 'react-toastify';

const CONTAINER_ID_TOAST = 'TOAST'
const CONTAINER_ID_ALERT = 'ALERT'

const configureToast = payload => ({
        ...payload,
        autoClose: true,
        type:payload.messageType  || 'default',
        position: toast.POSITION.BOTTOM_CENTER,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        containerId: CONTAINER_ID_TOAST,
        id:  payload.message.split(' ').join('-'),
        className:'Feedback',

    })

    const configureAlert = payload => ({
        ...payload,
        autoClose: false,
        type:payload.messageType || 'default',
        position:toast.POSITION.TOP_RIGHT,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        containerId: CONTAINER_ID_ALERT,
        id:  payload.message.split(' ').join('-'),
        className:'Feedback',

    })

function Toast(payload) {
    try {

        toast( payload.message, configureToast(payload))
   
    } catch ({ message, response }) {
        console.warn('[feedback.js]', { message, response })
    }
}

function Alert(payload) {
    try {
        
        toast(payload.message, configureAlert(payload))
   
    } catch ({ message, response }) {
        console.warn('[feedback.js]', { message, response })
    }
}



function* Feedback(data) {
    try {
        const { payload } = data

        if(!payload.type || !payload.message ){
            throw ({
                message:'Format feedback invalid',
                response:'The fields type and message are required'
            })
        }
     
        switch (payload.type) {
            case 'toast': Toast(payload); break;
            case 'alert': Alert(payload); break;
            default:
                break;
        }
   
    } catch ({ message, response }) {
        console.warn('[feedback.js]', { message, response })
    }
}
 
export default [
    takeEvery('FEEDBACK', Feedback),   
    takeEvery('FEEDBACK_ALERT', Alert),
    takeEvery('FEEDBACK_TOAST', Toast)
]    
   

