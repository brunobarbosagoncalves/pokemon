import { call, put, select, all, takeEvery } from 'redux-saga/effects';

function* getRootSquare() {
    try {
        //Call Api Promisse
        //const response = yield call({/* callApi promisse */ }, { /* object json params */ });

        //Saga select
        const counter = yield select(state=>state.counter.value)

        //Execute Action
        //yield put({ type: 'EVENT_SET_CATEGORIES', payload: {} });
   
        // if (response.status == 200 && response.data != null){
        //     yield put({ type: 'EVENT_SET_CATEGORIES', payloload: {} });
        // }

        yield put({ type: 'COUNTER_UPDATE', payload: counter*counter });
   
    } catch ({ message, response }) {
        console.warn('[event.js : getRootSquare]', { message, response })
    }
}

export default [
    takeEvery('COUNTER_GET_ROOT_SQUARE_X', getRootSquare),
]    
   

