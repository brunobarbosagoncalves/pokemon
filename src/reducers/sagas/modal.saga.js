import { call, put, select, takeEvery, delay } from "redux-saga/effects"

const DELAY_MILESECONDS = 2000

function* waitLoadModal() {
  try {
    //Delay N seconds
    yield delay(DELAY_MILESECONDS)
    console.log('SSSSS')
    // call dispatch to stop load modal
    yield put({ type: "MODAL_CLOSE_LOAD" })
  } catch ({ message, response }) {
    console.warn("[modal.saga.js : waitLoadModal]", { message, response })
  }
}

export default[
  takeEvery("WAIT_LOAD_MODAL", waitLoadModal)
]
