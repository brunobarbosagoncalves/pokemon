import { call, put, select, takeEvery, fork } from 'redux-saga/effects';

//import { ChangeLanguage } from 'i18n'

import moment from 'moment'

function* changeLanguage(action) {
    try {
     
        //Select instance i18n
        const i18nInstance = yield select(state => state.i18n.instance )
        
        //Change language in instance
        i18nInstance.changeLanguage(action.payload)
        
        //Change language in moment
        moment.locale(action.payload)

        //Change language in store
        yield put({ type: 'I18N_SET_LANGUAGE', payload: action.payload });
        
   
    } catch ({ message, response }) {
        console.warn('[i18n.saga.js : getLanguages]', { message, response })
    }
}


export default [
    takeEvery('I18N_CHANGE_LANGUAGES', changeLanguage),
]    
   

