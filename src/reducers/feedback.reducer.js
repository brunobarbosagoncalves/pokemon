const FEEDBACK_SET = "FEEDBACK_SET"

const initialState = {
  count: 0,
}

export default function alert(state = initialState, action = {}) {
  switch (action.type) {
    case FEEDBACK_SET:
      return Object.assign({}, state, { count: action.payload.count} )
    default:
      return state
  }
}