import counterReducer from './counter.reducer'
import i18nReducer from './i18n.reducer'
import modalReducer from './modal.reducer'
import feedbackReducer from './feedback.reducer'
import userReducer from './user.reducer'

//import { OtherReducer } from './other.reducer';

export default {
  counter: counterReducer,
  i18n: i18nReducer,
  modal: modalReducer,
  feedback: feedbackReducer,
  user: userReducer,
  //otherState: otherReducer
}
