const USER_SET_NEW = 'USER_SET_NEW'
const USER_SET_CLEAR = 'USER_SET_CLEAR'

const initialState = {
  username: '',
  password: '',
}

export default (state = initialState, action) => {
  switch (action.type) {
    case USER_SET_NEW:
      return { ...state, ...action.payload }
    case USER_SET_CLEAR:
      return { ...initialState }
    default:
      return state
  }
}
