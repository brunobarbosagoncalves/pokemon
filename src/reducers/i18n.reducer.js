import i18n from "../i18n"

const I18N_SET_LANGUAGE = "I18N_SET_LANGUAGE"
const I18N_SET_INSTANCE = "I18N_SET_INSTANCE"

const languages = Object.keys(i18n.options.resources) || []

const initialState = {
  instance: i18n,
  language: i18n.language,
  languages
}

export default (state = initialState, action) => {
  switch (action.type) {
    case I18N_SET_INSTANCE:
      return { ...state, instance: action.payload }
    case I18N_SET_LANGUAGE:
      return { ...state, language: action.payload }
    default:
      return state
  }
}
