//Actions name
const COUNTER_CLEAN = 'COUNTER_CLEAN'
const COUNTER_SET = 'COUNTER_SET' 
const COUNTER_SET_DYNAMIC = 'COUNTER_SET_DYNAMIC' 

const initialState = {
    value: 0,
    value_dynamic:0
};

export default (state = initialState, action) => {
    switch (action.type) {
        case COUNTER_CLEAN:
            return { ...initialState };
        case COUNTER_SET:
            return { ...state, value: action.payload };
        case COUNTER_SET_DYNAMIC:
            return { ...state, value_dynamic: action.payload };
        default:
            return state;
    }
};