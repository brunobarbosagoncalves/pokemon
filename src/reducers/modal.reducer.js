const MODAL_OPEN = "MODAL_OPEN"
const MODAL_CLOSE = "MODAL_CLOSE"
const MODAL_CLEAR = "MODAL_CLEAR"
const MODAL_ERROR_OFF = "MODAL_ERROR_OFF"
const MODAL_ERROR_ON = "MODAL_ERROR_ON"
const MODAL_LOADING_OFF = "MODAL_LOADING_OFF"
const MODAL_LOADING_ON = "MODAL_LOADING_ON"

const initialState = {
  modalName: "",
  modalOpen: false,
  modalError: false,
  modalLoading: true,
  payload: {},
}

export default function modal(state = initialState, action = {}) {
  switch (action.type) {
    case MODAL_OPEN:
      return Object.assign({}, state, {
        modalName: action.modalName,
        modalOpen: true,
        modalError: false,
        modalLoading: false,
        payload: action.payload || {}
      })
    case MODAL_CLOSE:
      return Object.assign({}, state, {
        modalName: "",
        modalOpen: false,
        modalError: false,
        modalLoading: false,
        payload: {}
      })
    case MODAL_ERROR_ON:
      return Object.assign({}, state, { modalError: true })
    case MODAL_ERROR_OFF:
      return Object.assign({}, state, { modalError: false })
    case MODAL_LOADING_ON:
      return Object.assign({}, state, { modalLoading: true })
    case MODAL_LOADING_OFF:
      return Object.assign({}, state, { modalLoading: false })
    case MODAL_CLEAR:
      return initialState
    default:
      return state
  }
}
