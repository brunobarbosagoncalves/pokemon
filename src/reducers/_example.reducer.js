//Actions name
const COUNTER_UPDATE = 'COUNTER_UPDATE' 
const COUNT_MORE = 'COUNT_MORE'
const COUNT_LESS = 'COUNT_LESS'
const COUNT_MORE_DYNAMIC = 'COUNT_MORE_DYNAMIC'
const COUNT_LESS_DYNAMIC = 'COUNT_LESS_DYNAMIC'

const initialState = {
    value: 0
};

export default (state = initialState, action) => {
    switch (action.type) {
        case COUNTER_UPDATE:
            return { ...state, value: action.payload };
        case COUNT_MORE:
            return { ...state, value: state.value+=1 };
        case COUNT_LESS:
            return { ...state, value: state.value-=1 };
        case COUNT_MORE_DYNAMIC:
            return { ...state, value: state.value+=parseInt(action.payload) };
        case COUNT_LESS_DYNAMIC:
            return { ...state, value: state.value-=parseInt(action.payload) };
        default:
            return state;
    }
};