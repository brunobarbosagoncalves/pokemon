import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'

import Button from 'components/Button'
import Grid from '@material-ui/core/Grid'
import Layout from 'components/Layout'
import styled, { css } from 'styled-components'
import Typography from 'components/Typography'

import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'

import COLORS from 'constants/colors'
import IMAGES from 'constants/images'

const PokeCard = styled(Grid).attrs({})`
  // border: 2px solid gray;
  // background-color: white;
  // border-radius: 20rem;
  // height: 7rem !important;
  width: 7rem !important;
  margin: .5rem .5rem;
  // padding: .5rem;
  cursor: pointer;
  // margin: .5rem !important;
  text-align: center;
  //width: calc(100% / 10) !important;

  // background: url(${IMAGES.POKEBOLA}) no-repeat fixed;
  // background-repeat: no-repeat;
  // background-attachment: fixed;
  // background-position: center center;

  :hover {
    border-color: red;
  }
`

const PokeImage = styled.img`
  width: 10rem;
  height: 10rem;
`

const LinkStyled = styled(Link)`
  color: ${COLORS.BLACK} !important;
  text-decoration: none !important;
  background-color: transparent !important;

  :hover {
    color: ${COLORS.BLACK} !important;
    text-decoration: none !important;
    background-color: transparent !important;
  }
`

const Details = props => {
  const [loading, setLoading] = useState(true)
  const [pokemonId, setPokemonId] = useState(props.match.params.id)
  const [error, setError] = useState(false)
  const [pokemon, setPokemon] = useState([])

  const HandlerChangeLoading = () => {
    setLoading(!loading)
  }

  const LoadPokemons = () =>
    axios
      .get(`https://pokeapi.co/api/v2/pokemon/${pokemonId}`)
      .then(result => {
        setPokemon(result.data)
        console.log(result)
        setLoading(false)
      })
      .catch(err => {
        setError(true)
      })

  useEffect(() => {
    LoadPokemons()
    //effect
    return () => {
      //cleanup
    }
  }, [])

  if (error) {
    return (
      <Layout sidemenu={'account_box'}>
        <Typography align="center" variant="h5">
          Ops...
        </Typography>
        <Typography align="center" variant="h5">
          Parece que essa página não existe ou ocorreu um erro
        </Typography>
        <LinkStyled to={'/'}>{'Home'}</LinkStyled>
      </Layout>
    )
  }

  if (loading) {
    return (
      <Layout sidemenu={'account_box'}>
        <Typography align="center" variant="h5">
          Loading
        </Typography>
      </Layout>
    )
  }

  return (
    <Layout sidemenu={'account_box'}>
      <Typography align="left" variant="h5">
        <Link to={`/`}>Pokemons</Link> > Details
      </Typography>
      <Grid container justifyContent="center" alignItems="center"></Grid>
      {/* Pagination start */}
      <Grid container direction="row" justify="center" alignItems="center">
        <Grid item xs={12}>
          <Typography align="center" variant="h4">
            {pokemon.name}
          </Typography>
        </Grid>

        <PokeImage src={pokemon.sprites.front_shiny} />

        {/* session hability */}
        {pokemon.abilities.length && (
          <Grid spacing={12} container content="start-flex" alignItems="center" xs={3}>
            <Grid container xs={12}>
              Habilidades
            </Grid>
            <FormControl style={{ width: '90%' }}>
              <Select value={'Filmes'} onChange={e => e.target.value}>
                {pokemon.abilities.map(data => (
                  <MenuItem value={data.ability.name}>{data.ability.name}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
        )}

        {/* session movie */}
        {pokemon.moves.length && (
          <Grid spacing={12} container content="start-flex" alignItems="center" xs={3}>
            <Grid container xs={12}>
              Golpes
            </Grid>
            <FormControl style={{ width: '90%' }}>
              <Select value={'Filmes'} onChange={e => e.target.value}>
                {pokemon.moves.map(data => (
                  <MenuItem value={data.move.name}>{data.move.name}</MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
        )}
      </Grid>
      {/* Pagination end */}
    </Layout>
  )
}

export default Details
