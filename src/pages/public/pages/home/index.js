import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'

import Button from 'components/Button'
import Grid from '@material-ui/core/Grid'
import Layout from 'components/Layout'
import styled, { css } from 'styled-components'
import Typography from 'components/Typography'
import Box from 'components/Box'
import Icon from 'components/Icon'

import COLORS from 'constants/colors'
import IMAGES from 'constants/images'

const PokeCard = styled(Grid).attrs({})`
  // border: 2px solid gray;
  // background-color: white;
  // border-radius: 20rem;
  // height: 7rem !important;
  width: 7rem !important;
  margin: .5rem .5rem;
  // padding: .5rem;
  cursor: pointer;
  // margin: .5rem !important;
  text-align: center;
  //width: calc(100% / 10) !important;

  // background: url(${IMAGES.POKEBOLA}) no-repeat fixed;
  // background-repeat: no-repeat;
  // background-attachment: fixed;
  // background-position: center center;

  :hover {
    border-color: red;
  }
`

const LinkStyled = styled(Link)`
  color: ${COLORS.BLACK} !important;
  text-decoration: none !important;
  background-color: transparent !important;

  :hover {
    color: ${COLORS.BLACK} !important;
    text-decoration: none !important;
    background-color: transparent !important;
  }
`

const BoxStyled = styled(Box)`
  margin-top: 2rem;
`

const ButtonStyled = styled(Button)`
  min-width: 12rem !important;
`
const Home = props => {
  const [loading, setLoading] = useState(true)
  const [pagination, setPagination] = useState({ page: 1, limit: 60, offset: 0, count: 964 })
  const [error, setError] = useState(false)
  const [pokemonList, setPokemonList] = useState([])

  const HandlerChangeLoading = () => {
    setLoading(!loading)
  }

  const LoadPokemonsDetails = () =>
    axios
      .get(
        `https://pokeapi.co/api/v2/pokemon?limit=${pagination.limit}&offset=${pagination.offset}`
      )
      .then(result => {
        console.log(result)
        setPokemonList(result.data.results)
        setLoading(false)
      })
      .catch(err => {
        setError(true)
      })

  useEffect(() => {
    LoadPokemonsDetails()
    //effect
    return () => {
      //cleanup
    }
  }, [pagination])

  const NextPage = () => {
    setLoading(true)
    setPagination({
      ...pagination,
      page: pagination.page + 1,
      offset: pagination.offset + pagination.limit,
    })
  }

  const PreviousPage = () => {
    setLoading(true)
    setPagination({
      ...pagination,
      page: pagination.page - 1,
      offset: pagination.offset - pagination.limit,
    })
  }

  if (error) {
    return (
      <Layout sidemenu={'account_box'}>
        <Typography variant="h3" align="center">
          Ops...
        </Typography>
      </Layout>
    )
  }

  if (loading) {
    return (
      <Layout sidemenu={'account_box'}>
        <Typography variant="h3" align="center">
          Loading
        </Typography>
      </Layout>
    )
  }

  return (
    <Layout sidemenu={'account_box'}>
      <Typography align="left" variant="h5">
        Pokemons
      </Typography>
      <Grid container direction="row" justify="space-between" alignItems="center">
        {pokemonList.map((poke, index) => (
          <PokeCard key={index} container direction="row" justify="center" alignItems="center">
            <Grid item container justify="center" aligItems="center">
              <LinkStyled to={`/details/${poke.url.split('/')[6]}/${poke.name}`}>
                <Typography pokeballName>{poke.name}</Typography>
              </LinkStyled>
            </Grid>
          </PokeCard>
        ))}
      </Grid>
      {/* Pagination start */}
      <BoxStyled mt={3}>
        <Grid spacing={6} container direction="row" justify="center" alignItems="center">
          <Grid item>
            <ButtonStyled
              startIcon={<Icon icon="chevron_left" />}
              onClick={PreviousPage}
              disabled={pagination.page === 1 ? true : false}
              variant="contained"
              color={COLORS.WHITE}
              bgcolor={COLORS.BLUE}
              type="button"
            >
              Previous page
            </ButtonStyled>
          </Grid>
          <Grid item>
            {' '}
            <ButtonStyled
              endIcon={<Icon icon="chevron_right" />}
              onClick={NextPage}
              disabled={pagination.offset + pagination.limit >= pagination.count}
              variant="contained"
              color={COLORS.WHITE}
              bgcolor={COLORS.BLUE}
              type="button"
            >
              Next page
            </ButtonStyled>
          </Grid>
        </Grid>
      </BoxStyled>
      {/* Pagination end */}
    </Layout>
  )
}

export default Home
