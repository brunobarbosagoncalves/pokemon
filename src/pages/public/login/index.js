import React, { Fragment, useEffect, useState } from 'react'

import { shallowEqual, useSelector, useDispatch } from 'react-redux'
import ImageBackground from 'components/ImageBackground'
import Image from 'components/Image'
import Container from 'components/Container'
import Typography from 'components/Typography'
import Grid from 'components/Grid'
import IMAGES from 'constants/images'

import FormLogin from './form'

const LoginPage = props => {
  const dispatch = useDispatch()

  const { i18nSel, languageSel, languagesSel, userSel } = useSelector(
    state => ({
      i18nSel: state.i18n.instamce,
      languageSel: state.i18n.language,
      languagesSel: state.i18n.languages,
      userSel: state.user,
    }),
    shallowEqual
  )

  const handlerRecoveryLogin = data => {
    console.log('RECOVERY::', data)
  }

  const handlerSubmit = data => {
    console.log('SUBMIT::', data)
  }

  const handlerStoreChange = payload => {
    dispatch({
      type: 'USER_SET_NEW',
      payload: { ...payload },
    })
    console.log('handlerStoreChange')
  }

  return (
    <Container style={{ height: '100vh' }}>
      <Grid
        container
        heightinherit
        wrap="nowrap"
        direction="column"
        justify="center"
        alignItems="center"
      >
        <Grid item>
          <Image height={15} src={IMAGES.LOGO_VERTICAL_EINSTEIN_BLUE} />
        </Grid>
        <Grid item>
          <Typography variant="h4" color="blue" align="center">
            Telespecialidades
          </Typography>
        </Grid>
        {/* <Image 
              title='Logo Einstein'
              //height='10rem'
              src={IMAGES.LOGO_VERTICAL_EINSTEIN_BLUE} 
              alt={'My Image'}
            /> */}
        {/* <p>{JSON.stringify(userSel)}</p> */}
        <Grid item>
          <button
            type="button"
            onClick={() => handlerStoreChange({ ...userSel, password: Math.random() })}
          >
            Change
          </button>
        </Grid>
        <Grid item>
          <FormLogin
            {...props}
            handlerRecoveryLogin={handlerRecoveryLogin}
            onSubmit={handlerSubmit}
            data={userSel}
            closeModal={() => {}}
          />
        </Grid>
      </Grid>
    </Container>
  )
}

export default LoginPage
