import React from 'react'

import { Formik, Form, Field, ErrorMessage } from 'formik'
import * as Yup from 'yup'

import InputFormControl from 'components/InputFormControl'
import InputText from 'components/InputText'
import Button from 'components/Button'

const SignupSchema = Yup.object().shape({
  username: Yup.string()
    .min(2, 'Too Short!')
    .max(70, 'Too Long!')
    .email('Invalid email')
    .required('Required'),
  password: Yup.string()
    .min(2, 'Too Short!')
    .max(70, 'Too Long!')
    .required('Required'),
})

const ButtonCustom = props => {
  return <button>OLA</button>
}

const formik = props => {
  console.log('formik props::', props)
  // const modalProps = props.payload
  // props.payload = JSON.parse(modalProps)
  console.log('formik props after::', props.payload)

  return (
    <Formik
      enableReinitialize
      initialValues={{
        ...props.data,
      }}
      validationSchema={SignupSchema}
      onSubmit={values => {
        console.log('VALUES BEFORE SEND', values)
        const { username, password } = values

        // same shape as initial values
        props.onSubmit({ username, password })
      }}
    >
      {({ values, errors, touched, isSubmitting, isValid, handleChange, ...rest }) => (
        <Form>
          <InputFormControl>
            <Field
              id="name"
              component={InputText}
              type={'text'}
              name="username"
              title={'Email do usuário'}
              placeholder={'Digite seu email'}
            />
            <ErrorMessage name="username" />
          </InputFormControl>

          <Field
            adornmentLeft={ButtonCustom}
            // adornmentRight={ButtonCustom}
            iconRight={ButtonCustom}
            id="name"
            component={InputText}
            type={'text'}
            name="password"
            title={'Senha do usuario'}
            placeholder={'Digite sua senha'}
          />
          <ErrorMessage name="password" />

          <Button
            onClick={() => props.handlerRecoveryLogin(values.username)}
            type="button"
            buttomsimple
            text={''}
          >
            Recuperar senha
          </Button>
          <Button type="submit" disabled={!isValid /*|| isSubmitting */ || !touched} text={'Login'}>
            Login
          </Button>
        </Form>
      )}
    </Formik>
  )
}

export default formik
