import styled from "styled-components"

const ContainerSplash = styled.div`
  display: flex;
  height: -webkit-fill-available;
  justify-content: center;
  align-items: center;
`
const ContentContainerSplash = styled.div`
  font-size: 5rem;
  color: red;
`

export { ContainerSplash, ContentContainerSplash }
