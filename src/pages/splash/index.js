import React, { Component } from "react"
import { connect } from "react-redux"

/* local import */
import { ContainerSplash, ContentContainerSplash } from "./styles"

class Splash extends Component {
  state = {
    loading: true
  }

  render() {
    if (this.state.loading) {
      setInterval(this.setState({ loading: false }), 2000)
    }
    /* Waiting Load */
    return this.state.loading ? (
      <ContainerSplash>
        <ContentContainerSplash>Loading . . .</ContentContainerSplash>
      </ContainerSplash>
    ) : (
      <div>{this.props.children}</div>
    )
  }
}

const mapStateToProps = state => ({
  // language: state.traductor.language
})

const mapDispatchToProps = dispatch => ({})

export default connect(mapStateToProps, mapDispatchToProps)(Splash)
