#As páginas são organizadas por níveis

pages
    #Not login
    public  #Guest
        pages
            #site
            Home
                index.js
            #Site/FAQ
            FAQ
                index.js
    #site/ (With Login)
    private #User
        pages
            Post
                components
                    ComponentLocalName
                        index.js
                pages
                    #site/post/:idPost/add    
                    Add
                        index.js
                    Remove
                        index.js
                    Update
                        index.js
                    ...
    #site/admin
    Admin   #User Admin Session
        Components
        Acount