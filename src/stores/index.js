/* imports */
import { 
  createStore, 
  applyMiddleware, 
  compose, 
  combineReducers 
} from 'redux'
import createSagaMiddleware from 'redux-saga'

/* local imports */
import  Reducers  from '../reducers';
import rootSaga from '../reducers/sagas'
import modalSaga from 'reducers/sagas/modal.saga'

// Middlewares
const sagaMiddleware = createSagaMiddleware();

//Mount Store
const store = createStore(
    combineReducers(Reducers),
    compose( 
      applyMiddleware(sagaMiddleware),
      window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()  
    ),
    
)

//Execute and on saga Listen events
sagaMiddleware.run(rootSaga);

export default store