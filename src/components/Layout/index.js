import React, { useEffect, useState } from 'react'

import styled from 'styled-components'
import PropTypes from 'prop-types'
import CssBaseline from '@material-ui/core/CssBaseline'

import AppBar from 'components/AppBar'
import Drawer from '@material-ui/core/Drawer'

import Hidden from 'components/Hidden'
import Toolbar from 'components/Toolbar'
import Typography from 'components/Typography'
import Icon from 'components/Icon'
import Grid from 'components/Grid'
import Image from 'components/Image'

import SideNav from './sidenav'

import COLORS from 'constants/colors'
import IMAGES from 'constants/images'

const drawerWidth = 0

const LayoutContainer = styled('div')`
  .MuiDrawer-paperAnchorDockedLeft {
    border-right: 1px solid ${COLORS.BLUE} !important;
  }

  .root-container {
    display: flex;
  }

  .drawer {
    width: 4rem;

    @media (max-width: 600px) {
      width: ${drawerWidth}px;
      flex-shrink: 0;
    }
  }

  .appBar {
    width: ${p => p.windowWidth}px;
    padding-left: ${drawerWidth}px;
    background-color: ${COLORS.BLUE};

    @media (max-width: 600px) {
      width: ${p => p.windowWidth}px;
      padding-left: 0.5rem;
    }
  }
  .menuButton {
    marginright: theme.spacing(2), @meida (min-width: 601px) {
      display: none;
    }
  }

  .content {
    width: 100%;
    flex-grow: 1;
    padding: 2rem;
    padding-top: 5rem;

    //mob
    @media (max-width: 600px) {
      padding-top: 3.5rem;
    }
  }

  .;
`

const ImagePerfilBox = styled.div.attrs(p => ({
  title: p.title || '',
}))`
  padding-left: .5rem,
  height:100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

const ResponsiveDrawerCustom = props => {
  const drawerWidth = 60
  const GetWidth = () =>
    window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth

  const [menuOpen, setMenuOpen] = useState(false)
  const [mobileOpen, setMobileOpen] = useState(false)

  let [widthScreenLeftDrawer, setWidthScreenLeftDrawer] = useState(GetWidth())

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen)
    setMenuOpen(!menuOpen)
  }

  useEffect(() => {
    const EventWindowResize = () => {
      setWidthScreenLeftDrawer(GetWidth())
    }

    EventWindowResize()

    //add listen on event
    window.addEventListener('resize', EventWindowResize)

    return () => {
      EventWindowResize()
      //remove listen on event
      window.removeEventListener('resize', EventWindowResize)
    }
  })

  console.log('sideMenu', props.sidemenu)

  return (
    <LayoutContainer windowWidth={widthScreenLeftDrawer} className={'root-container:'}>
      <CssBaseline />
      <AppBar position="fixed" className={'appBar'}>
        <Toolbar>
          <Grid container>
            <Grid item xs>
              <Grid h100 container justify="start-flex" alignItems="center">
                <Grid item xs>
                  <Typography h100 variant="subtitle1" noWrap>
                    Pokemon
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>

      <main className={'content'}>
        {/* content page bottom */}
        {props.children}
        {/* content page top */}
      </main>
    </LayoutContainer>
  )
}

ResponsiveDrawerCustom.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  container: PropTypes.any,
}

export default ResponsiveDrawerCustom
