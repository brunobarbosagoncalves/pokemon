import React from 'react'

import styled from 'styled-components';

const DrawerHeaderStyled = styled('div')
.attrs({})`
  height: 8rem;
  width: 100%;
  border: 1px solid green;
`

const DrawerHeaderCustom = props => 
  <DrawerHeaderStyled {...props} className='d-flex justify-content-center align-items-center'>
    <React.Fragment>  
      { props.children }
    </React.Fragment>
  </DrawerHeaderStyled>

export default DrawerHeaderCustom