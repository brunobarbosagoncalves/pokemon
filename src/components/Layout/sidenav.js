import React, { useState, useEffect } from 'react'

import List from 'components/List'
import ListItem from 'components/ListItem'
import ListItemIcon from 'components/ListItemIcon'
import ListItemText from 'components/ListItemText'
import Divider from '@material-ui/core/Divider'
import styled, { css } from 'styled-components'
import Icon from 'components/Icon'
import COLORS from 'constants/colors'

import InboxIcon from '@material-ui/icons/MoveToInbox'
import MailIcon from '@material-ui/icons/Mail'

const DrawerStyled = styled('div').attrs({})`
  width: 60px !important;
  overflow: hidden;

  .MuiDrawer-paperAnchorDockedLeft {
    border-right: 1px solid ${COLORS.BLUE} !important;
  }

  ${p =>
    p.menuOpen === true &&
    css`
      width: 240px !important;
      overflow: auto;
    `}
`
const DrawerCustom = props => {
  const [open, setOpen] = useState(props.open)
  //const menuOpen = props.menuOpen

  useEffect(() => {
    //effect
    return () => {
      //cleanup
    }
  })

  const handleDrawerClose = () => {
    props.onClose()
    setOpen(false)
  }

  const ListMenuIcon = styled(List)`
    background-color: ${COLORS.BLUE};
    min-height: 64px !important;
    display: flex;
    justify-content: center;
    align-items: center;

    @media (max-width: 600px) {
      min-height: 30px !important;
    }
  `

  const MenuIcon = props => {
    const icon = props.menuOpen ? 'menu_open' : 'menu'
    return (
      <React.Fragment>
        <ListMenuIcon nopadding>
          <ListItem sideMenu button {...props}>
            <Icon icon={icon} sideMenu cursorPointer color="white" />
            <ListItemText sideMenu primary={'Menu'} color={'white'} />
          </ListItem>
        </ListMenuIcon>
        <Divider />
      </React.Fragment>
    )
  }

  let menuSideInfos = [
    { icon: 'today', name: 'Agenda', selected: false },
    { icon: 'account_box', name: 'Paciente', selected: false },
    { icon: 'settings', name: 'Cadastro', selected: false },
    { icon: 'insert_chart', name: 'Dashboard', selected: false },
  ]

  menuSideInfos = menuSideInfos.map(item =>
    props.sidemenu === item.icon ? { ...item, selected: true } : item
  )

  return (
    <DrawerStyled {...props} variant="persistent" anchor="left" open={open}>
      <MenuIcon {...props} onClick={handleDrawerClose} />
      <List nopadding>
        {menuSideInfos.map((item, index) => (
          <React.Fragment>
            <ListItem selected={props.sidemenu === item} sideMenu button key={item}>
              <Icon sideMenu icon={item.icon} selected={props.sidemenu === item} />
              <ListItemText
                sideMenu
                hidden={!props.menuOpen}
                primary={item.name}
                selected={props.sidemenu === item}
              />
            </ListItem>
            <Divider />
          </React.Fragment>
        ))}

        {
          // <React.Fragment>
          //   <ListItem sideMenu selected button key={'email'}>
          //     <Icon sideMenu selected icon={'email'} />
          //     <ListItemText sideMenu selected hidden={!props.menuOpen} primary={'email'} />
          //   </ListItem>
          // </React.Fragment>
        }
      </List>
    </DrawerStyled>
  )
}

export default DrawerCustom
