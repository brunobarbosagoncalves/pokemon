import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import styled from 'styled-components';

const FormControlStyled = styled(FormControl)
.attrs({})`

`

const InputSelectCustom = props =>{
  
  return (
      <FormControlStyled {...props}>
        { props.children }  
      </FormControlStyled>
  );
}

export default InputSelectCustom
