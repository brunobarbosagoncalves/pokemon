import React from 'react'

import styled, { css } from 'styled-components'
import Button from '@material-ui/core/Button'

const ButtonStyled = styled(Button).attrs({})`
  ${p =>
    p.color &&
    css`
      color: ${p.color} !important;
    `}

  ${p =>
    p.bgcolor &&
    css`
      background-color: ${p.bgcolor} !important;
    `}
`

const ButtonCustom = props => {
  return <ButtonStyled {...props}>{props.children}</ButtonStyled>
}

export default ButtonCustom
