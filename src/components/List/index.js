import React from 'react'
import styled, { css } from 'styled-components'
import PropTypes from 'prop-types'
import List from '@material-ui/core/List'

const ListStyled = styled(List).attrs({})`
  ${p =>
    p.nopadding &&
    css`
      padding: 0px !important;
    `}
`

const ListCustom = props => {
  const { children } = props
  return <ListStyled {...props}>{children}</ListStyled>
}

export default ListCustom
