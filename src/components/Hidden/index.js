import React from 'react'
import styled, { css } from 'styled-components'
import Hidden from '@material-ui/core/Hidden'

const HiddenStyled = styled(Hidden).attrs({})``

const HiddenCustom = props => {
  const { children, ...rest } = props
  return <HiddenStyled {...rest}>{children}</HiddenStyled>
}

export default HiddenCustom
