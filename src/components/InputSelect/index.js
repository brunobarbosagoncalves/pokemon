import React from 'react';

import styled from 'styled-components';
import NativeSelect from '@material-ui/core/NativeSelect';

const InputSelectStyled = styled(NativeSelect)
.attrs({})`

`

const InputSelectCustom = props =>{

  const [value, setValue] = react.useState(props.selected || '')

  const inputLabel = React.useRef(null);
  const [labelWidth, setLabelWidth] = React.useState(0);
  React.useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, []);

  const handleChange = ev => setValue(ev.target.value)

  const { 
      options = [], 
      inputProps = {}, 
      ...restProps 
    } = props

  return (
    <InputSelectStyled
        value={value}
        onChange={handleChange}
        inputProps={inputProps}
        { ...restProps }
    >
        {
            options.map(option =>
                <option key={option.value || Math.random()} value={option.value} {...option.props || ''} >
                    {option.text}
                </option>)
        }
    </InputSelectStyled>
  );
}

export default InputSelectCustom
