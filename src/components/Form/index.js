import React from 'react'

import { Formik, useField, useForik, Form, Field } from 'formik'
import * as Yup from 'yup'
import PropTypes from 'prop-types'

import InputText from 'components/InputText'

const MyForm = props => {
  const formik = useFormik({
    initialValues: {
      firstName: '',
      lastName: '',
      email: '',
    },
    onSubmit: values => {
      alert(JSON.stringify(values, null, 2))
    },
  })

  return (
    <div />
    // <Form onSubmit={formik.onSubmit}>
    //   <Field id={'name'} name={'name'} component={InputText} />
    // </Form>
  )
}

export default MyForm
