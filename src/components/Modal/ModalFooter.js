import React from 'react';

import MuiDialogActions from '@material-ui/core/DialogActions';
import styled from 'styled-components';
import PropTypes from 'prop-types';


const ModalFooterStyled = styled(MuiDialogActions)
.attrs({})`

`
const ModalFooterCustom = props => {
  const { children } = props
    return(
        <ModalFooterStyled {...props}>
            <React.Fragment>
                { children } 
            </React.Fragment>
        </ModalFooterStyled>
    )
}

ModalFooterCustom.propTypes={
    children: oneOfType([
        PropTypes.string,
        PropTypes.node,
        PropTypes.arrayOf(PropTypes.node),
    ]),
}

ModalFooterCustom.defaultProps={
    children: 'Modal sem rodape',
}

export default ModalFooterCustom