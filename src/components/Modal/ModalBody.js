import React from 'react';

import MuiDialogContent from '@material-ui/core/DialogContent';
import styled from 'styled-components';
import PropTypes from 'prop-types';


const ModalBodyStyled = styled(MuiDialogContent)
.attrs({})`

`
const ModalBodyCustom = props => {
  const { children } = props
    return(
        <ModalBodyStyled {...props}>
            <React.Fragment>
                { children } 
            </React.Fragment>
        </ModalBodyStyled>
    )
}

ModalBodyCustom.propTypes={
    children: oneOfType([
        PropTypes.string,
        PropTypes.node,
        PropTypes.arrayOf(PropTypes.node),
    ]),
}

ModalBodyCustom.defaultProps={
    children: 'Modal sem corpo',
}

export default ModalBodyCustom