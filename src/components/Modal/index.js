import Modal from 'components/Modal/Modal'
import ModalHeader from 'components/Modal/ModalHeader'
import ModalBody from 'components/Modal/ModalBody'
import ModalFooter from 'components/Modal/ModalFooter'

export {
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter
}