import React from 'react';

import Dialog from '@material-ui/core/Dialog';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const ModalStyled = styled(Dialog)
.attrs({})`

`
const ModalCustom = props => {

    const { open, onClose, onSuccess, onError, children } = props

    const [open, setOpen] = React.useState(open);

    return(
        <ModalStyled onSuccess={onSuccess} onError={onError} onClose={onClose} open={open} {...props}>
            <React.Fragment>
                { children } 
            </React.Fragment>
        </ModalStyled>
    )
}

ModalCustom.propTypes={
    children: oneOfType([
        PropTypes.string,
        PropTypes.node,
        PropTypes.arrayOf(PropTypes.node),
    ]),
    onClose: PropTypes.func.isRequired,
    onSuccess: PropTypes.func.isRequired,
    onError: PropTypes.func.isRequired,
}

ModalCustom.defaultProps={
    children: 'Modal sem header, corpo e rodape',
    onClose: ()=>setOpen(!open),
}

export default ModalCustom