import React from 'react';

import MuiDialogTitle from '@material-ui/core/DialogTitle'
import IconButton from '@material-ui/core/IconButton';
import styled from 'styled-components';
import PropTypes from 'prop-types';


const ModalHeaderStyled = styled(MuiDialogTitle)
.attrs({})`

`
const ModalHeaderCustom = props => {

    const { children, onClose, variantTitle, closeIcon: CloseIcon, ...other } = props;

    const closeButton = onClose ? 
        <IconButton aria-label="close" onClick={onClose}> <CloseIcon /> </IconButton> : <div />
  
    return(
        <ModalHeaderStyled {...props}>
            <Typography variant={variantTitle}>{children}</Typography>
            { closeButton }
        </ModalHeaderStyled>
    )
}

ModalHeaderCustom.propTypes={
    onClose:oneOfType([
        PropTypes.bool,
        PropTypes.func
    ]),
    children: oneOfType([
        PropTypes.string,
        PropTypes.node,
        PropTypes.arrayOf(PropTypes.node),
    ]),
    closeIcon: oneOfType([
        PropTypes.node,
        PropTypes.bool,
    ]),
    variantTitle: PropTypes.string
}

ModalHeaderCustom.defaultProps={
    onClose: false,
    children: 'Modal sem titulo',
    closeIcon: false,
    variantTitle: 'h6'
}

export default ModalHeaderCustom