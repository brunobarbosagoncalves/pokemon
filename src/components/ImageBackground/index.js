import React from 'react'

import styled, { css } from 'styled-components'
import PropTypes from 'prop-types'

const ImageBackgroundStyled = styled('div').attrs({})`
  ${p =>
    p.backgroundImage &&
    css`
      background-image: ${p.backgroundImage};
    `}

  ${p =>
    p.backgroundImageType === 'fullscreen' &&
    css`
      background-repeat: no-repeat;
      background-size: contain;

      /* Set up proportionate scaling */
      width: 100% !important;
      height: auto !important;

      /* Set up positioning */
      position: fixed !important;
      top: 0 !important;
      left: 0 !important;
    `}

    ${p =>
      p.heightfull &&
      css`
        height: 100% !important;
      `}
`

const ImageBackgroundCustom = props => {
  const { backgroundImage, backgroundImageType, children } = props
  console.log('props', props)
  return <ImageBackgroundStyled {...props}>{children}</ImageBackgroundStyled>
}

ImageBackgroundCustom.propTypes = {
  backgroundImage: PropTypes.string.isRequired,
  backgroundImageType: PropTypes.oneOf(['fullscreen', 'horizontal', 'vertical']).isRequired,
  children: PropTypes.node,
}

export default ImageBackgroundCustom
