import React from 'react'
import styled, { css } from 'styled-components'
import Grid from '@material-ui/core/Grid'

const GridStyled = styled(Grid).attrs({})`
  ${p =>
    p.heightfull &&
    css`
      height: 100% !important;
    `}
  ${p =>
    p.heightinherit &&
    css`
      height: inherit !important;
    `}

    ${p =>
      p.h100 &&
      css`
        height: 100% !important;
      `}
`

const GridCustom = p => <GridStyled {...p}>{p.children}</GridStyled>

export default GridCustom
