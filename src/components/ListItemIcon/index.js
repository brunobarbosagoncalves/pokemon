import React from 'react'
import styled, { css } from 'styled-components'
import PropTypes from 'prop-types'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import COLORS from 'constants/colors'

const ListItemIconStyled = styled(ListItemIcon).attrs({})`
  ${p =>
    p.color &&
    css`
      color: ${p.color} !important;
    `}

  ${p =>
    p.selected &&
    css`
      color: ${COLORS.BLUE} !important;
    `}

    ${p =>
      p.sideMenu &&
      css`
        width: 50px !important;
      `};
`

const ListItemIconCustom = props => {
  const { children } = props
  return <ListItemIconStyled {...props}>{children}</ListItemIconStyled>
}

export default ListItemIconCustom
