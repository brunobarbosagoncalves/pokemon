import React from 'react'
import styled, { css } from 'styled-components'
import PropTypes from 'prop-types'
import ListItemText from '@material-ui/core/ListItemText'
import COLORS from 'constants/colors'

const ListItemTextStyled = styled(ListItemText).attrs({})`
  display: block;

  ${p =>
    p.hidden &&
    css`
      display: nono;
    `}

  ${p =>
    p.sideMenu &&
    p.selected &&
    css`
      color: ${COLORS.BLUE} !important;
    `}

    ${p =>
      p.sideMenu &&
      !p.selected &&
      css`
        color: gray !important;
      `}

    ${p =>
      p.color &&
      css`
        color: ${p.color} !important;
      `}
`

const ListItemTextCustom = props => {
  const { children } = props
  return <ListItemTextStyled {...props}>{children}</ListItemTextStyled>
}

export default ListItemTextCustom
