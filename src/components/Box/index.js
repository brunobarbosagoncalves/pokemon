import React from 'react'

import styled, { css } from 'styled-components'
import Box from '@material-ui/core/Box'

const BoxStyled = styled(Box).attrs({})``

const BoxCustom = props => {
  return <BoxStyled {...props}> {props.children} </BoxStyled>
}

export default BoxCustom
