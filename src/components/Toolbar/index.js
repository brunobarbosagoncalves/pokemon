import React from 'react'
import Toolbar from '@material-ui/core/Toolbar'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

const ToolbarStyled = styled(Toolbar).attrs({})``

const ToolbarCustom = props => {
  const { children } = props
  return (
    <ToolbarStyled {...props}>
      <React.Fragment>{children}</React.Fragment>
    </ToolbarStyled>
  )
}

ToolbarCustom.propTypes = {
  children: PropTypes.oneOf([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
}

export default ToolbarCustom
