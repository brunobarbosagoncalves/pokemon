import React from 'react';

import styled from 'styled-components'
import Card from '@material-ui/core/Card';

const CardStyled = styled(Card)
.attrs({})`

`
const CardCustom = props => {
  return (
    <CardStyled {...props}>
     { props.children }
    </CardStyled>
  )
}

export default CardCustom