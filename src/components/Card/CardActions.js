import React from 'react';

import styled from 'styled-components'
import CardActions from '@material-ui/core/CardActions';

const CardActionsStyled = styled(CardActions)
.attrs({})`

`

const CardActionsCustom = props => {
  return (
    <CardActionsStyled {...props}>
        {props.children}
    </CardActionsStyled>
  )
}

export default CardActionsCustom