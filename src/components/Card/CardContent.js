import React from 'react';

import styled from 'styled-components'
import CardContent from '@material-ui/core/CardContent';

const CardContentStyled = styled(CardContent)
.attrs({})`

`

const CardContentCustom = props => {
  return (
    <CardContentStyled {...props}>
        {props.children}
    </CardContentStyled>
  )
}

export default CardContentCustom