import React from 'react'
import styled, { css } from 'styled-components'
import AppBar from '@material-ui/core/AppBar'

const AppBarStyled = styled(AppBar).attrs({})``

const AppBarCustom = props => {
  const { children, ...rest } = props
  return <AppBarStyled {...rest}>{children}</AppBarStyled>
}

export default AppBarCustom
