import React from 'react'
import styled, { css } from 'styled-components'
import PropTypes from 'prop-types'
import ListItem from '@material-ui/core/ListItem'
import COLORS from 'constants/colors'

const ListItemStyled = styled(ListItem).attrs({})`
  border-left: 3px solid transparent !important;
  ${p =>
    p.sideMenu &&
    css`
      height: 3.5rem;
      display: flex;
      justify-content: left;
      align-items: center;
    `}
  ${p =>
    p.sideMenu &&
    p.selected &&
    css`
      border-left: 3px solid ${COLORS.BLUE} !important;
      padding-left: -3px;
    `};
`

const ListItemCustom = props => {
  const { children } = props
  return <ListItemStyled {...props}>{children}</ListItemStyled>
}

export default ListItemCustom
