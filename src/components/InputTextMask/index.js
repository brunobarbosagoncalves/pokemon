import React from 'react';

import MaskedInput from 'react-text-mask';
import PropTypes from 'prop-types';
import { styled } from '@material-ui/core';

const InpuTextMaskStyled = styled(MaskedInput)
.attr({})`

`

const InpuTextMaskCustom = props => {
  const { 
      inputRef, 
      mask, 
      ...restProps 
    } = props;


  return (
    <InpuTextMaskStyled
      {...restProps}
      ref={ref => { inputRef(ref ? ref.inputElement : null); }}
      //mask={['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
      //showMask
    />
  );
}

InpuTextMaskCustom.propTypes = {
    mask: PropTypes.object.isRequired,
    showMask: PropTypes.bool.isRequired,
  };

export default InpuTextMaskCustoms