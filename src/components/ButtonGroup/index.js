import React from 'react';

import styled from 'styled-components'
import ButtonCustom from 'components/Button';

const ButtonGroupStyled = styled(ButtonCustom)
.attrs({})`

`

const ButtonGroupCustom = props => {
  
  return (
      <ButtonGroupStyled {...props}>
        <React.Fragment>
            { props.children }
        </React.Fragment>
      </ButtonGroupStyled>
  );
}

export default ButtonGroupCustom
