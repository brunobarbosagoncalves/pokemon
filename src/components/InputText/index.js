import React from 'react'

import styled, { css } from 'styled-components'
import { TextField } from '@material-ui/core'
import InputAdornment from '@material-ui/core/InputAdornment'
import PropTypes from 'prop-types'

const InputTextStyled = styled(TextField).attrs(props => ({}))`
  ${p =>
    p.error &&
    css`
    
      }
    `}

  ${p =>
    p.success &&
    css`
      & + ::after {
        border-bottom: 1px solid #2196f3 !important;
        padding: 5rem !important;
      }
    `}
`

const InputTextCustom = props => {
  const {
    title,
    placeholder,
    field,
    form: { errors, touched },
    adornmentLeft: ElemenLeft,
    adornmentRight: ElemenRight,
  } = props

  const inputProps = props.inputProps || {}

  if (ElemenLeft) {
    inputProps.startAdornment = (
      <InputAdornment position="start">
        <ElemenLeft />
      </InputAdornment>
    )
  }

  if (ElemenRight) {
    inputProps.endAdornment = (
      <InputAdornment position="end">
        <ElemenRight />
      </InputAdornment>
    )
  }

  return (
    <InputTextStyled
      {...props}
      {...field}
      error={errors[field.name] && touched[field.name]}
      success={!errors[field.name] && touched[field.name]}
      InputProps={inputProps}
      title={title}
      placeholder={placeholder}
    />
  )
}

InputTextCustom.propTypes = {
  title: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
}

export default InputTextCustom
