import React from 'react';

import InputLabel from '@material-ui/core/InputLabel';
import styled from 'styled-components';

const InputLabelStyled = styled(InputLabel)
.attrs({})`

`

const InputLabelCustom = props => {
  return (
    <InputLabelStyled htmlFor={props.htmlfor || props.id} {...props}>
      { props.children }
    </InputLabelStyled>
  );
}

export default InputLabelCustom
