import React from 'react'
import styled, { css } from 'styled-components'
import Icon from '@material-ui/core/Icon'
import PropTypes from 'prop-types'
import COLORS from 'constants/colors'

const IconBoxStyled = styled('div').attrs(p => ({}))`
  font-size: ${p => p.fontSize && p.fontSize};
`
const IconStyled = styled(Icon).attrs(p => ({}))`

  font-size: ${p => p.fontSize && p.fontSize};
  cursor: default;
  display: flex;
  

  ${p =>
    p.appBarMobileMenu &&
    css`
      padding-right: 2.5rem;
      cursor: pointer;
    `}
    
  ${p =>
    p.cursorPointer &&
    css`
      cursor: pointer;
    `}

  ${p =>
    p.sideMenu &&
    css`
      display: inline-flex;
      min-width: 50px;
      flex-shrink: 0;
    `}

  ${p =>
    p.sideMenu &&
    p.selected &&
    css`
      font-size: 5rem;
      color: ${COLORS.BLUE} !important;
    `}
    
    ${p =>
      p.sideMenu &&
      !p.selected &&
      css`
        font-size: 5rem;
        color: gray !important;
      `}

  ${p => p.iconNavbarHeader && css``}


  //last override
  ${p =>
    p.color &&
    css`
      color: ${p.color} !important;
    `}
`
const IconCustom = props => {
  const { icon, iconStyle, boxStyle } = props

  return (
    <IconBoxStyled style={boxStyle} {...props}>
      <IconStyled style={iconStyle} {...props}>
        {icon}
      </IconStyled>
    </IconBoxStyled>
  )
}

IconCustom.propTypes = {
  icon: PropTypes.string.isRequired,
}

IconCustom.defaultProps = {
  iconStyle: {},
  boxStyle: {},
}

export default IconCustom
