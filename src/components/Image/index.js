import React from 'react'
import styled, { css } from 'styled-components'
import PropTypes from 'prop-types'

const ImageStyled = styled('img').attrs({})`


  ${p =>
    p.navbarperfil &&
    css`
      border-radius: 100rem;
      border: 0.1rem solid white;
      height: ${p.height}rem;
    `}

  ${p =>
    p.height &&
    css`
      height: ${p.height}rem !important;
    `}

  ${p =>
    p.width &&
    css`
      width: ${p.width}rem !important;
    `}

  ${p =>
    p.h100 &&
    css`
      height: 100% !important;
    `}

  ${p =>
    p.w100 &&
    css`
      width: 100% !important;
    `}

  ${p =>
    p.responsive &&
    css`
      width: 100%;
      height: auto;
    `}
  ${p =>
    p.radius100 &&
    css`
      border-radius: 100rem;
    `}



      
`

const ImageCustom = p => {
  const { src } = p
  return <ImageStyled {...p} />
}

ImageCustom.propTypes = {
  src: PropTypes.string.isRequired,
  height: PropTypes.number.isRequired,
}

export default ImageCustom
