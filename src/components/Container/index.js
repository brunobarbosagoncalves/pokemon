import React from 'react'

import styled from 'styled-components'

const ContainerImageStyled = styled('div').attrs({})``

const ContainerImageCustom = props => (
  <ContainerImageStyled {...props}>{props.children}</ContainerImageStyled>
)

export default ContainerImageCustom
