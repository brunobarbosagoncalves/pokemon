import React from 'react'

import Typography from '@material-ui/core/Typography'
import styled, { css } from 'styled-components'
import PropTypes from 'prop-types'

const TypographyStyled = styled(Typography).attrs(p => ({
  title: p.title,
}))`
  //override
  ${p =>
    p.color &&
    css`
      color: ${p.color} !important;
    `}

  ${p =>
    p.size &&
    css`
      font-size: ${p.size}rem !important;
    `}

  ${p =>
    p.h100 &&
    css`
      height: 100% !important;
    `}

    ${p =>
      p.pokeballName &&
      css`
        width: 100%;
        min-width: 6rem;
        border: 2px solid gray;
        padding: 0.2rem;
        border-radius: 5rem;

        :hover {
          border-color: red;
        }
      `}

`

const TypographyCustom = p => {
  const { children, title } = p
  return (
    <TypographyStyled {...p}>
      <React.Fragment>{children}</React.Fragment>
    </TypographyStyled>
  )
}

TypographyCustom.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.element, PropTypes.string]),
}

export default TypographyCustom
