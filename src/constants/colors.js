export default {
  RED_LOGO: '#FF0016',
  BLUE: '#006EBD',
  BLUE_DARK: '#025891',
  RED: '#F02020',
  GREEN: '#4CAF50',
  WHITE: 'white',
  BLACK: 'black',
}
