import React, { useState } from 'react';

import PropTypes from 'prop-types';
import styled from 'styled-components'
import { Col, Row } from 'reactstrap'
import IMAGES from 'constants/images'
import COLORS from 'constants/colors'
import Image from 'components/Image'
import { 
    Nav, 
    NavbarBrand, 
    NavItemList, 
    NavItem, 
    Dropdown, 
    DropdownItem 
} from 'containers/Navbar';
import { FaUserCircle } from 'react-icons/fa'

const HeaderStyled =  styled('div')
.attrs({})`

`

const HeaderCustom = props => {
    const { 
        params={}
    } = props

    return(
        <Nav className='row'>
            <div className='w-100'>
                <NavbarBrand>   
                    <Image imageHeader src={IMAGES.LOGO_HORIZONTAL_EINSTEIN_WHITE} alt='dd' />
                </NavbarBrand>

                <div className='d-flex align-items-lg-center justify-content-center'>
                    <div className='text-white'>TELE ESPECIALIDADES</div>
                </div>

                <div className='ml-auto'>
                    <NavItemList right color='white' title='juhu' className='ml-auto w-100 d-flex justify-content-md-around align-items-center'>
                        <Dropdown className='' right color='white' title={"Perfil medico sus  "} right>
                            <DropdownItem title={'item list'} href={'http://www.uol.com.br'}/>
                            <DropdownItem title={'item list'} href={'http://www.uol.com.br'}/>
                            <DropdownItem title={'item list'} href={'http://www.uol.com.br'}/>
                            <DropdownItem title={'item list'} href={'http://www.uol.com.br'}/>
                            <DropdownItem title={'item list'} href={'http://www.uol.com.br'}/>
                        </Dropdown>
                        <button>huhuh</button>
                        <button>huhuh</button>
                        <button>huhuh</button>
                        <button>huhuh</button>
                        <button>huhuh</button>
                        <button>huhuh</button>
                    </NavItemList>
                </div>
                   
            </div>
        </Nav> 
    )
}

HeaderCustom.propTypes ={
    params: PropTypes.object,
}

export default HeaderCustom