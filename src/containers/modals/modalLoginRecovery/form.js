import React, { Component } from "react"

import { connect } from "react-redux"
import { Formik, Form, Field, Label } from "formik"
import * as Yup from "yup"
import PropTypes from "prop-types"
import { Col, Row } from "reactstrap"
import Button from 'components/Button'
import Hr from 'components/Hr'

import { InputTextMask, InputText, ErrorMessage, Input } from "components/Form"

import MASKS from "constants/masks"

const SignupSchema = Yup.object().shape({
  username: Yup.string()
    .min(2, "Too Short!")
    .max(70, "Too Long!")
    .email("Invalid email")
    .required("Required")
})

const formik = props => {
        
    return(
        <Formik
        enableReinitialize 
        initialValues={{
          username: ''
          //username: props.username || props.payload.username || ''
        }}
        validationSchema={SignupSchema}
        onSubmit={values => {
            const { username } = values
            props.cbSuccess({username})
        }}
      >
        {({ values, errors, touched, isSubmitting, isValid, ...rest }) => (
          <Form>
            <Row>
              <Col className="col-12 col-sm-12 col-md-6 col-lg-12">
                <Field
                  component={Input}
                  type={'text'}
                  name="username"
                  title={'Email do usuário'}
                  placeholder={'Digite seu email'}
                />
                <ErrorMessage name="username" />
              </Col>
              <Hr nomargintop />
              <Col className="col-12 col-sm-12 col-md-6 col-lg-12">
                <div className="d-flex justify-content-around align-items-center">
                    <Button onClick={props.modalClose} type="button" buttomsimple text={'Sair'} />
                    <Button type="submit" disabled={!isValid || isSubmitting || !touched } text={'Enviar'} />
                </div>
              </Col>
            </Row>
          </Form>
        )}
      </Formik>
    )
}

formik.defaultProps = {
  cbFail: () => {}
}

formik.propTypes = {
  cbSuccess: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  language: state.i18n.language,
  i18n: state.i18n.instance
})

const mapDispatchToProps = state => ({})

export default connect(mapStateToProps, mapDispatchToProps)(formik)
