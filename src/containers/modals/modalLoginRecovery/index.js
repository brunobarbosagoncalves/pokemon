import React,{ useState, useEffect } from "react"
import { connect } from 'react-redux'
import  IMAGES from 'constants/images'

import { Col } from 'reactstrap'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'containers/modals/ModalParts'

import FormLogin from './form'

import { Input } from 'components/Form'
import Label from 'components/Label'
import Image from 'components/Image'
import Button from 'components/Button'
import FormGroup from 'components/FormGroup'

const MODAL_NAME = "MODAL_LOGIN_RECOVERY"

const ModalLogin = props => {

  const [username, setUsername] = useState(props.payload.username || '')

  useEffect(() => {
      loadDataInit()
      
  },[])

  const handlerRecoveryLogin = ()=>{ 
    
  }

  const toggle = () => {
    if (props.modalOpen) {

      props.modalClose()
    }
  }

  /**
   * @description Call any Endpoint to populate data
   */
  const loadDataInit = () => {
    console.log('Load data init') 
  }

  const callbackDefault = data =>{
    console.log(`${MODAL_NAME}, not have callback >  ${JSON.stringify(data)}`)
  }

  const handlerLogin = async () => {

    //open loading
    props.modalLoadingOn()

    // remove fields modal
    const { payload:{ callback, ...restData} } = props
    
    //make data
    const data = Object.assign({username}, restData)

    //if exist callback function received props.payload 
    if(callback){
      try{
        return callback(data)
      }catch(err){
        console.log(`${MODAL_NAME}: erro: ${JSON.stringify(err)}`)    
        props.modalErrorOn()
        return false
      }
    }

    callbackDefault(data)

    toggle()
  }


  
  const successModal = async data => {
    //call loading
    props.modalLoadingOn()

    console.log('DATA::', data)

    //If a MODAL is UNcontrolled
    //props.callback(data)

    //IF MODAL CONTROLLED

    //try action on Promisse
    // const response = await MyPromiseToSave(data)

    //if response OK close modal and continue flux
    //props.modalClose()
    //Call Toast

    //If response erro resend data from modal using params and reopen
    // const data = { modalName:'MyModalName', data }
    //props.modalReOpen(data)
  }

  const data ={
      username
  }

  return(
    <Modal isOpen={props.modalOpen} toggle={toggle} size="md">
      <ModalHeader modalprops={props} toggle={toggle}>Recuperar senha</ModalHeader>

      <ModalBody modalprops={props}>

        <Col>
          <p 
            className="d-flex justify-content-center align-items-center" 
            style={{height:'5rem'}}>
            Instruções para recuperar senha do usuário
            </p>
        </Col>
        
        <FormLogin {...{data:username}}{...props} cbSuccess={successModal} />

      </ModalBody>
      <ModalFooter modalprops={props}>
        <Button backgroundcolor='red'  text={'Recuperar senha'} onClick={handlerRecoveryLogin }/>
        <Button text={'Entrar'} onClick={handlerLogin }/>
      </ModalFooter>
    </Modal>
  )
} 

const mapStateToProps = ({ modal }) => ({
  modalOpen: modal.modalName == MODAL_NAME && modal.modalOpen,
  modalName: modal.modalName,
  modalError: modal.modalError,
  modalLoading: modal.modalLoading,
  payload: modal.payload
})

const mapDispatchToProps = dispatch => ({
  modalClose() {
    dispatch({ type: "MODAL_CLOSE" })
  },
  modalLoadingOn() {
    dispatch({ type: "MODAL_LOADING_ON" })
  },
  modalLoadingOff() {
    dispatch({ type: "MODAL_LOADING_OFF" })
  },
  modalErrorOn() {
    dispatch({ type: "MODAL_ERROR_ON" })
  },
  modalErrorOff() {
    dispatch({ type: "MODAL_ERROR_OFF" })
  },
  modalReOpen(data) {
    dispatch({ type: "MODAL_OPEN", modalName:data.modalName, payload: data.data })
  },
  
})

export default connect(mapStateToProps, mapDispatchToProps)(ModalLogin)
