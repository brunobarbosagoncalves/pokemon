import Modal from './Modal'
import ModalHeader from './ModalHeader'
import ModalBody from './ModalBody'
import ModalBodyError from './ModalBodyError'
import ModalBodyLoading from './ModalBodyLoading'
import ModalFooter from './ModalFooter'

export {
    Modal,
    ModalHeader,
    ModalBody,
    ModalBodyError,
    ModalBodyLoading,
    ModalFooter
}