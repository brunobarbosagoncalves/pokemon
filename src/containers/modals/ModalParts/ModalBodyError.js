import React from 'react'
import { ModalBody } from 'reactstrap';
import styled from 'styled-components'
import { FadeIn } from 'animate-css-styled-components';


const ModalBodyErrorCustom = styled(ModalBody).attrs({})`
    height:18rem;
    transition: opacity 2s;
    opacity: 1;
`

const ModalBodyError = props => 
    //<FadeIn duration="2s" delay="0s"  >
        <ModalBodyErrorCustom>
            <div className="d-flex flex-column h-100 align-items-center justify-content-center text-center">
                <h2>Ops... houve um error</h2>
                <br />
                <p>
                    Mas não se preoucupe, estamos enviando esse incidente nesse 
                    exato momento para os nosso time de suporte, logo estaremos corrigindo
                    <br /><br />
                    Equipe de suporte Albert Einstein
                </p>
            </div>
        </ModalBodyErrorCustom>
    //</FadeIn>
    
    
export default ModalBodyError