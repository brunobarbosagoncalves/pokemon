import React from 'react'

import { ModalBody } from 'reactstrap';
import styled,{css} from 'styled-components'
import { FadeIn } from 'animate-css-styled-components';
import { ModalBodyError, ModalBodyLoading } from './index'
import Call from 'wrappers/requester'

const ModalBodyStyledCustom = styled(ModalBody).attrs({})`

`
const sendReportError = (modalprops) => {
    //process.env.DEBUG_MODAL_ERROR || false
    if(true){
        const data = {
            location:{},
            modal:{},
            navigator:{}
        }

        //get data navigator
        for (var key in window.navigator) {
            var valueKey = window.navigator[key]
            if ( typeof valueKey !== 'function' && valueKey !== {}  && valueKey !== undefined) {
                data['navigator'][key]=valueKey
            }
        }
        //get data modal
        for (var key in modalprops) {
            var valueKey = modalprops[key]
            if ( typeof valueKey !== 'function' && valueKey !== undefined) {
                data['modal'][key]=valueKey
            }
        }
        //get data location 
        data['location']=window.location
        
        if(process.env.DEBUG_MODAL_ERROR || true){
            console.log(modalprops.modalName , data)
        }

        console.log('SEND CRASH MODAL DATA', data)
        // Call('post','URL DESTINY',{data})

    }
}

const ModalBodyCustom = props => {

    const { modalprops:{ modalError, modalLoading } } = props

    //If modal.reducer modalError:true || modalLoading:true  
    if(modalError){
        sendReportError(props.modalprops)
        return <ModalBodyError modalprops={props.modalprops}/> 
    }

    if(modalLoading){
        return <ModalBodyLoading modalprops={props.modalprops}/>
    }

    return( 
        //<FadeIn duration="1s" delay="0s"  >
            <ModalBodyStyledCustom {...props}>
                {props.children}
            </ModalBodyStyledCustom>
        //</FadeIn>
    )
}

export default ModalBodyCustom