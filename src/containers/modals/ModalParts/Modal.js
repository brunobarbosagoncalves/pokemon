import React from 'react'

import { Modal } from 'reactstrap';

import styled,{css} from 'styled-components'

const ModalStyled = styled(Modal).attrs({})``

function ModalCustom(props){
    return(
        <ModalStyled 
            {...props}>
            {props.children}
        </ModalStyled>
    )
}

export default ModalCustom