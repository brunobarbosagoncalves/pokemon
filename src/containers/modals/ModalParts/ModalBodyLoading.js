import React from 'react'
import { ModalBody } from 'reactstrap';
import styled from 'styled-components'
import { FadeOut } from 'animate-css-styled-components';

const ModalBodyLoadingCustom = styled(ModalBody).attrs({})`
    height:18rem;
    transition: opacity 2s;
    opacity: 1;
`

const ModalBodyLoading = props =>
    //<FadeOut duration="2s" delay="1s">
        <ModalBodyLoadingCustom>
            <div className="d-flex h-100 align-items-center justify-content-center">
                <h1 style={{paddingRight:'.5rem'}}>Loading </h1>
                <div className="spinner-grow text-dark" style={{width: "1.5rem", height: "1.5rem",marginRight:'.8rem'}} role="status" />
                <div className="spinner-grow text-dark" style={{width: "1.5rem", height: "1.5rem",marginRight:'.8rem'}} role="status" />
                <div className="spinner-grow text-dark" style={{width: "1.5rem", height: "1.5rem",marginRight:'.8rem'}} role="status" />
            </div>
        </ModalBodyLoadingCustom>
    //</FadeOut>

export default ModalBodyLoading