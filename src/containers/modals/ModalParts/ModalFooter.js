import React from 'react'

import { ModalFooter } from 'reactstrap';
import styled,{css} from 'styled-components'
import { FadeIn } from 'animate-css-styled-components';

const ModalFooterStyledCustom = styled(ModalFooter).attrs({})`

`

function ModalFooterCustom(props){

    const { modalprops:{ modalError, modalLoading } } = props

    //If modal.reducer modalError:true || modalLoading:true  
    if(modalError || modalLoading){
        return( <div /> )
    }

    return( 
        <FadeIn duration="3s" delay="0s" style={{display:'none'}} >
            <ModalFooterStyledCustom {...props}>
                {props.children}
            </ModalFooterStyledCustom>
        </FadeIn>
    )
}

export default ModalFooterCustom