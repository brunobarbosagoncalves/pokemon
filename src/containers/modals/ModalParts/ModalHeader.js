import { ModalHeader } from 'reactstrap';

import styled,{css, keyframes} from 'styled-components'

const pulse = keyframes`
    0% {
        transform: scale(0.95);
        box-shadow: 0 0 0 0 red;
    }

    70% {
        transform: scale(1);
        box-shadow: 0 0 0 10px rgba(0, 0, 0, 0);
        filter: brightness(1);
    }

    100% {
        transform: scale(0.95);
        box-shadow: 0 0 0 0 rgba(0, 0, 0, 0);
    }
`;

const ModalHeaderCustom = styled(ModalHeader)`

    text-align: center;

    .modal-title{
        width: 100%;
    }

    button.close {
        opacity:1 !important; 
    }
    
    button.close:focus {
        opacity:1 !important;
        background-color: whote !important;
        border: 0px !important;
        outline: 0px !important; 
    }
    
    button.close > span{
        color: black;
        // font-size: 2rem;
        // position: absolute;
        // top: -1.3rem;
        // right: -1.3rem;
        // background-color: white;
        // border-radius: 10rem;
        // padding:.2rem;
        // height: 3rem;
        // width: 3rem;
        // opacity: 1;
        // z-index: 501;
        
        // border-right: 2px solid rgba(0,0,0,.2);
        // border-top: 2px solid rgba(0,0,0,.2);

        // transition: background-color 1s ease-out;
        // transition: color 2s ease-out;
    }

    button.close > span:hover{
        // border: 2px solid red;
        // color: red;
        // animation: ${pulse} 1s infinite ;
        
    }

    //override
    ${props => props.hideclose && css`
        button.close{
            display: none !important;
        }
    `}
`

export default ModalHeaderCustom