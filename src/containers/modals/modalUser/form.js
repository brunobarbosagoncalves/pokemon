import React, { Component } from "react"

import { connect } from "react-redux"
import { Field, Form, Label } from "formik"
import { FormPlus } from 'components/Form'
import * as Yup from "yup"
import PropTypes from "prop-types"
import { Col, Row } from "reactstrap"
import Button from 'components/Button'
import Hr from 'components/Hr'

import { InputText, ErrorMessage, InputSelect } from "components/Form"

import MASKS from "constants/masks"

const SignupSchema = Yup.object().shape({
  email: Yup.string()
    .min(2, "Too Short!")
    .max(70, "Too Long!")
    .email("Invalid email")
    .required("Required"),
  password: Yup.string()
    .min(4, "Too Short!")
    .max(20, "Too Long!")
    .required("Required"),
  name: Yup.string()
    .min(3, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  crm: Yup.string()
    .min(0, "Too Short!")
    .max(6, "Too Long!"),
  username: Yup.string()
    .min(2, "Too Short!")
    .max(70, "Too Long!")
    .required("Required"),
  specialityId: Yup.string()
    .max(70, "Too Long!"),
  userTypeId: Yup.string()
    .max(70, "Too Long!"),
  addressId: Yup.string()
    .max(70, "Too Long!"),
  scheduleId: Yup.string()
    .max(70, "Too Long!"),
})

const formik = props => {
    
    const flagoperation = props.formflag 

    const initialValues = {
      username:     props.username || props.payload.username || '',
      password:     props.password || props.payload.password || '',
      email:        props.email || props.payload.email || '',
      name:         props.name || props.payload.name || '',
      specialityId: props.specialityId || props.payload.specialityId || '',
      userTypeId :  props.userTypeId || props.payload.userTypeId || '',
      scheduleId :  props.scheduleId || props.payload.scheduleId || '',
    }

    return(
        <FormPlus
        {...props}
        enableReinitialize 
        initialValues={initialValues}
        validationSchema={SignupSchema}
        //callback={}
        onSubmit={values => {
            // const { 
            //   username, password, name, email, 
            //   specialityId, userTypeId, scheduleId 
            // } = values
            
            props.formcallback({ ...values })
        }}
      >
        {({ values, errors, touched, isSubmitting, isValid, handleChange, handleBlur, ...rest }) => (
            <Form>
              <Col className="col-12 col-sm-12 col-md-6 col-lg-12">
                <Field
                  flagoperation={flagoperation}
                  component={InputText}
                  type={'text'}
                  name="name"
                  title={'Nome do usuário'}
                  placeholder={'Digite seu nome'}
                />
                <ErrorMessage flagoperation={flagoperation} name="name" />
              </Col>

              <Col className="col-12 col-sm-12 col-md-6 col-lg-12">
                <Field
                  flagoperation={flagoperation}
                  component={InputText}
                  type={'text'}
                  name="username"
                  title={'Username do usuário'}
                  placeholder={'Digite seu username'}
                />
                <ErrorMessage flagoperation={flagoperation} name="username" />
              </Col>

              <Col className="col-12 col-sm-12 col-md-6 col-lg-12">
                <Field
                  flagoperation={flagoperation}
                  component={InputText}
                  type={'text'}
                  name="email"
                  title={'Email do usuário'}
                  placeholder={'Digite seu email'}
                />
                <ErrorMessage flagoperation={flagoperation} name="email" />
              </Col>

              <Col className="col-12 col-sm-12 col-md-6 col-lg-12">
                <Field
                  flagoperation={flagoperation}
                  component={InputText}
                  type={'password'}
                  name="password"
                  title={'Senha do usuário'}
                  placeholder={'Digite sua senha'}
                />
                <ErrorMessage flagoperation={flagoperation} name="password" />
              </Col>

              <Col className="col-12 col-sm-12 col-md-6 col-lg-12">
                <Field
                  flagoperation={flagoperation}
                  component={InputSelect}
                  id='specialityId'
                  type={'select'}
                  name="specialityId"
                  options={[{id:1,name:'AAAA'},{id:2,name:'BBB'}, {id:3,name:'CCCC'}]}
                  title={'Especialidade do usuario'}
                  placeholder={'Selecione a especialidade'}
                />
                <ErrorMessage flagoperation={flagoperation} name="specialityId" />
              </Col>

              <Col className="col-12 col-sm-12 col-md-6 col-lg-12">
                <Field
                  flagoperation={flagoperation}
                  component={InputSelect}
                  type={'select'}
                  id={"userTypeId"}
                  name="userTypeId"
                  options={[{id:1,name:'Medico'},{id:2,name:'Gestor'}, {id:3,name:'Atendente'}]}
                  title={'Tipo do usuario'}
                  placeholder={'Selecione o tipo de usuario'}
                />
                <ErrorMessage flagoperation={flagoperation} name="userTypeId" />
              </Col>

              <Col className="col-12 col-sm-12 col-md-6 col-lg-12">
                <Field
                  flagoperation={flagoperation}
                  component={InputSelect}
                  type={'select'}
                  options={[{id:1,name:'AAAA'},{id:2,name:'BBB'}, {id:3,name:'CCCC'}]}
                  name="scheduleId"
                  id={"scheduleId"}
                  title={'Agenda do usuario'}
                  placeholder={'Selecione a agenda do usuario'}
                />
                <ErrorMessage name="scheduleId" />
              </Col>
              <Hr nomargintop />
              <Col className="col-12 col-sm-12 col-md-6 col-lg-12">
                <div className="d-flex justify-content-around align-items-center">
                    <Button onClick={props.modalClose} type="button" buttomsimple={'true'} text={'Sair'} />
                    <Button type="submit" disabled={!isValid || isSubmitting || !touched} text={'Enviar'} />
                </div>
              </Col>
            </Form>
        )}
      </FormPlus>
    )
}

const mapStateToProps = state => ({
  language: state.i18n.language,
  i18n: state.i18n.instance
})

const mapDispatchToProps = state => ({})

export default connect(mapStateToProps, mapDispatchToProps)(formik)
