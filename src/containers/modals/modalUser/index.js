import React,{ useState, useEffect } from "react"

import { connect } from 'react-redux'
import FormUser from './form'
import Button from 'components/Button'
import { Col } from 'reactstrap'
import { 
  Modal, 
  ModalHeader, 
  ModalBody, 
  ModalFooter 
} from 'containers/modals/ModalParts'



const MODAL_NAME = "MODAL_USER"

const ModalLogin = props => {

  const modalFieldOptions = props.payload.modalFieldOptions
  const flagoperation = props.payload.flagoperation

  const [username, setUsername] = useState(props.username || props.payload.username || '')
  const [email, setEmail] = useState(props.email || props.payload.email || '')
  const [name, setName] = useState(props.name || props.payload.name || '')
  const [password, setPassword] = useState(props.password || props.payload.password || '')
  const [specialityId, setSpecialityId] = useState(props.specialityId || props.payload.specialityId || '')
  const [userTypeId, setUserTypeId] = useState(props.userTypeId || props.payload.userTypeId || '')
  const [scheduleId, setScheduleId] = useState(props.scheduleId || props.payload.scheduleId || '')


  /*
    username, password, name, email, 
    specialityId, userTypeId, scheduleId  
  */
  useEffect(() => {
      loadDataInit()
      
  },[])

  const handlerRecoveryLogin = ()=>{ 
    
  }

  const toggle = () => {
    if (props.modalOpen) {

      props.modalClose()
    }
  }

  /**
   * @description Call any Endpoint to populate data
   */
  const loadDataInit = () => {
    console.log('Load data init') 
  }

  const callbackDefault = data =>{
    console.log(`${MODAL_NAME}, not have callback >  ${JSON.stringify(data)}`)
  }

  const handlerLogin = async () => {

    //open loading
    props.modalLoadingOn()

    // remove fields modal
    const { payload:{ callback, ...restData} } = props
    
    //make data
    const data = Object.assign({username}, restData)

    //if exist callback function received props.payload 
    if(callback){
      try{
        return callback(data)
      }catch(err){
        console.log(`${MODAL_NAME}: erro: ${JSON.stringify(err)}`)    
        props.modalErrorOn()
        return false
      }
    }

    callbackDefault(data)

    toggle()
  }


  
  const successModal = data => {
    //call loading
    props.modalLoadingOn()

    console.log('DATA::', data)

    //If a MODAL is UNcontrolled
    //props.callback(data)

    //IF MODAL CONTROLLED

    //try action on Promisse
    // const response = await MyPromiseToSave(data)

    //if response OK close modal and continue flux
    //props.modalClose()
    //Call Toast

    //If response erro resend data from modal using params and reopen
    // const data = { modalName:'MyModalName', data }
    //props.modalReOpen(data)
  }

  const data ={
      username
  }

  const dataModalOptions = {
    title:'',
    buttonLegend:''
  }

  switch(flagoperation){
    case 'add': 
      dataModalOptions.title='Adiciona novo usuario';
      dataModalOptions.buttonLegend='Adicionar'; 
      break;
    case 'edit': 
      dataModalOptions.title='Atualizar usuario';
      dataModalOptions.buttonLegend='Atualizar' 
      break;
    case 'remove': 
      dataModalOptions.title='Apagar usuario'; 
      dataModalOptions.buttonLegend='Apagar'
      break;
    case 'view': 
      dataModalOptions.title='Ver usuario';
      dataModalOptions.buttonLegend='Fechar' 
      break;
    default: 
      dataModalOptions.title='Usuario'
      dataModalOptions.buttonLegend='Seguir'
  }


  return(
    <Modal isOpen={props.modalOpen} toggle={toggle} size="md">
      <ModalHeader modalprops={props} toggle={toggle}>{dataModalOptions.title}</ModalHeader>

      <ModalBody modalprops={props}>

        <Col>
          <p 
            className="d-flex justify-content-center align-items-center" 
            style={{height:'5rem'}}>
            Instruções para recupara senha ao usuário
            </p>
        </Col>
        
        <FormUser  
          {...props}
          formflag={flagoperation}
          formparams={props.payload}
          formoptions={dataModalOptions}   
          formcallback={successModal}
          formfooter={<p1>ola</p1>} 
        />

      </ModalBody>
      <ModalFooter modalprops={props}>
        <Button backgroundcolor='red'  text={'Recuperar senha'} onClick={handlerRecoveryLogin }/>
        <Button text={'Entrar'} onClick={handlerLogin }/>
      </ModalFooter>
    </Modal>
  )
} 

const mapStateToProps = ({ modal }) => ({
  modalOpen: modal.modalName == MODAL_NAME && modal.modalOpen,
  modalName: modal.modalName,
  modalError: modal.modalError,
  modalLoading: modal.modalLoading,
  payload: modal.payload
})

const mapDispatchToProps = dispatch => ({
  modalClose() {
    dispatch({ type: "MODAL_CLOSE" })
  },
  modalLoadingOn() {
    dispatch({ type: "MODAL_LOADING_ON" })
  },
  modalLoadingOff() {
    dispatch({ type: "MODAL_LOADING_OFF" })
  },
  modalErrorOn() {
    dispatch({ type: "MODAL_ERROR_ON" })
  },
  modalErrorOff() {
    dispatch({ type: "MODAL_ERROR_OFF" })
  },
  modalReOpen(data) {
    dispatch({ type: "MODAL_OPEN", modalName:data.modalName, payload: data.data })
  },
  
})

export default connect(mapStateToProps, mapDispatchToProps)(ModalLogin)
