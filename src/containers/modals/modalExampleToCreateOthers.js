import React from "react"
import { connect } from "react-redux"
import { Modal, ModalHeader, ModalBody, ModalFooter, ModalBodyError } from './ModalParts'
import {  Button } from "reactstrap"

const MODAL_NAME = "MY_MODAL_LOGIN"

class ModalFeatureName extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      /* Local fields default */
      }

    //bind method here if he not use pattern es6(arrow function)
    this.handleChange = this.handleChange.bind(this)
    //call ever load first time on show
    this.loadDataInit()
  }

  // merge reducer.props and state props
  static getDerivedStateFromProps(props, state) {
    if (props.modalOpen && props.payload) {
      // Set default modal title
      state = Object.assign({}, state, props.payload)
    }
    
    state.modalOpen = props.modalOpen

    return state
  }

  loadDataInit = () => {
    /**
     *
     * Load Data Or Call Api Here
     *
     */
    //Close load modal
    //this.props.modalCloseLoad()
    setTimeout(()=>this.props.modalLoadingClose(),3000)
  }

  toggle = () => {
    if (this.props.modalOpen) {
      console.log("FECHAR MODAL")
      this.props.close()
    }
  }

  save = () => {
    // remove fields modal

    const { modalName, modalOpen, modalError, modalLoading, payload, ...restData } = this.state
    //make data
    const data = Object.assign({}, restData)

    console.log("DATA", data)

    // Set on parent page an close modal
    //this.props.params.callback(data)

    //close modal
    this.toggle()
  }

  handleChange = ev => {
    let { name, value } = ev.target

    if (name == "hide") {
      value = value == "0" ? false : true
    }

    this.setState({ [name]: value })
  }

  render() {

    if (this.props.modalLoading==true) {
      //setTimeout(this.setState({'modelLoading':false}), 2000)
    }

    return (
      <Modal isOpen={this.props.modalOpen} toggle={this.toggle} size="lg">
        <ModalHeader toggle={this.toggle}>Modal title</ModalHeader>

        <ModalBody modalloading={this.props.modalLoading} modalerror={this.props.modalError}>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
          ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
          ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur
          sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
          est laborum.
        </ModalBody>
        <ModalFooter modalloading={this.props.modalLoading} modalerror={this.props.modalError}>
          <Button color="primary" onClick={this.save}>
            SAVE{" "}
          </Button>{" "}
          <Button color="secondary" onClick={this.toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    )
  }
}

const mapStateToProps = ({ modal }) => ({
  modalOpen: modal.modalName == MODAL_NAME && modal.modalOpen,
  modalName: modal.modalName,
  modalError: modal.modalError,
  modalLoading: modal.modalLoading,
  payload: modal.payload
})

const mapDispatchToProps = dispatch => ({
  close() {
    dispatch({ type: "MODAL_CLOSE" })
  },
  modalLoadingClose() {
    dispatch({ type: "MODAL_LOADING_OFF" })
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(ModalFeatureName)
