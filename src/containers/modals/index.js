import React from "react"

import ModalLogin from "./modalLogin"
import ModalLoading from "./modalLoading"
import ModalLoginRecovery from './modalLoginRecovery'
import ModalUser from './modalUser'

export default class Modals extends React.Component {
  render() {
    return (
      <React.Fragment>
        <ModalLogin />
        <ModalLoading />
        <ModalLoginRecovery />
        <ModalUser />
      </React.Fragment>
    )
  }
}
