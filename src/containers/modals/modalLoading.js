import React from "react"
import { connect } from "react-redux"

import { Modal, ModalBody, ModalHeader } from './ModalParts'
import { ModalFooter, Button } from "reactstrap"

const MODAL_NAME = "MODAL_LOADING"

class ModalLoading extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      /* fields default */
      modalName: MODAL_NAME,
      modalOpen: false,
      modalError: false,
      modalLoading: false
      /* others props if necessary*/
    }

    //bind method here if he not use pattern es6(arrow function)
    this.handleChange = this.handleChange.bind(this)
    //call ever load first time on show
    this.loadDataInit()
  }

  static getDerivedStateFromProps(props, state) {
    if (props.modalOpen && props.payload) {
      // Set default modal title
      state = Object.assign({}, state, props.payload)
    }

    state.modalOpen = props.modalOpen

    return state
  }

  loadDataInit = () => {
    /**
     *
     * Load Data Or Call Api Here
     *
     */
    //Close load modal
    //this.props.modalCloseLoad()
  }

  toggle = () => {
    if (this.props.modalOpen) {
      console.log("FECHAR MODAL")
      this.props.close()
    }
  }

  save = () => {
    // remove fields modal

    const { modalName, modalOpen, modalError, modalLoading, payload, ...restData } = this.state
    //make data
    const data = Object.assign({}, restData)

    console.log("DATA", data)

    // Set on parent page an close modal
    //this.props.params.callback(data)

    //close modal
    this.toggle()
  }

  handleChange = ev => {
    let { name, value } = ev.target

    if (name == "hide") {
      value = value == "0" ? false : true
    }

    this.setState({ [name]: value })
  }

  render() {
    return (
      <Modal
        centered
        //isOpen={this.props.modalLoading}
        toggle={this.toggle}
        size="lg"
      >
        <ModalBody style={{ height:"18rem"}}>
          <div className="d-flex h-100 align-items-center justify-content-center">
            <h1>Loading . . .</h1>
          </div>
        </ModalBody>
      </Modal>
    )
  }
}

const mapStateToProps = ({ modal }) => ({
  modalOpen: modal.modalOpen,
  modalName: modal.modalName,
  modalError: modal.modalError,
  modalLoading: modal.modalLoading,
  payload: modal.payload
})

const mapDispatchToProps = dispatch => ({
  close() {
    dispatch({ type: "MODAL_CLOSE" })
  },
  login(data) {
    dispatch({ type: "USER_LOGIN", data })
  },
  modalCloseLoad() {
    dispatch({ type: "MODAL_LOAD_CLOSE" })
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(ModalLoading)
