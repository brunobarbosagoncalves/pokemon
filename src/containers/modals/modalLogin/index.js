import React,{ useState, useEffect } from "react"
import { connect } from 'react-redux'
import  IMAGES from 'constants/images'

import { Col } from 'reactstrap'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'containers/modals/ModalParts'

import FormLogin from './form'

import { Input } from 'components/Form'
import Label from 'components/Label'
import Image from 'components/Image'
import Button from 'components/Button'
import FormGroup from 'components/FormGroup'

const MODAL_NAME = "MODAL_LOGIN"

const ModalLogin = props => {

  const [username, setUsername] = useState(props.payload.username || '')
  const [password, setPassword] = useState(props.payload.password || '')



  useEffect(() => {
      loadDataInit()
    
  },[])

  const handlerRecoveryLogin = ()=>{ 
    alert('Forget password') 
  }

  const toggle = () => {
    if (props.modalOpen) {

      props.modalClose()
    }
  }

  /**
   * @description Call any Endpoint to populate data
   */
  const loadDataInit = () => {
    console.log('Load data init') 
  }

  const callbackDefault = data =>{
    console.log(`${MODAL_NAME}, not have callback >  ${JSON.stringify(data)}`)
  }

  const handlerLogin = async () => {

    //open loading
    props.modalLoadingOn()

    // remove fields modal
    const { payload:{ callback, ...restData} } = props
    
    //make data
    const data = Object.assign({username, password}, restData)

    //if exist callback function received props.payload 
    if(callback){
      try{
        return callback(data)
      }catch(err){
        console.log(`${MODAL_NAME}: erro: ${JSON.stringify(err)}`)    
        props.modalErrorOn()
        return false
      }
    }

    callbackDefault(data)

    toggle()
  }
  
  const sleep = (msecond) => new Promise( (res, rej)=>{ setTimeout( ()=>res('ssss'),msecond ) } )
  
  const successModal = async data => {
    props.modalLoadingOn()
    const r = await sleep(5000)

    console.log('Before call modal', data)
    props.modalClose()
    props.openModalLogin(data)
  }

  const data ={
      username,
      password
  }


  return(
    <Modal isOpen={props.modalOpen} toggle={toggle} size="md">
      <ModalHeader modalprops={props} toggle={toggle}>Login</ModalHeader>

      <ModalBody modalprops={props}>

        <Col className="text-center">
          <Image title='Logo Eins' height='10rem' src={IMAGES.LOGO_EINSTEIN} alt={'My Image'} />
        </Col>
        
        <FormLogin {...data}{...props} cbSuccess={successModal} />

      </ModalBody>
      <ModalFooter modalprops={props}>
        <Button backgroundcolor='red'  text={'Recuperar senha'} onClick={handlerRecoveryLogin }/>
        <Button text={'Entrar'} onClick={handlerLogin }/>
      </ModalFooter>
    </Modal>
  )
} 

const mapStateToProps = ({ modal }) => ({
  modalOpen: modal.modalName == MODAL_NAME && modal.modalOpen,
  modalName: modal.modalName,
  modalError: modal.modalError,
  modalLoading: modal.modalLoading,
  payload: modal.payload
})

const mapDispatchToProps = dispatch => ({
  modalClose() {
    dispatch({ type: "MODAL_CLOSE" })
  },
  login(data) {
    dispatch({ type: "USER_LOGIN", data })
  },
  modalLoadingOn() {
    dispatch({ type: "MODAL_LOADING_ON" })
  },
  modalLoadingOff() {
    dispatch({ type: "MODAL_LOADING_OFF" })
  },
  modalErrorOn() {
    dispatch({ type: "MODAL_ERROR_ON" })
  },
  modalErrorOff() {
    dispatch({ type: "MODAL_ERROR_OFF" })
  },
  openModalLogin(data) {
    console.log('PORPS IN OPEN', data)
    dispatch({ type: "MODAL_OPEN",modalName:'MODAL_LOGIN', payload: data })
  },
  
})

export default connect(mapStateToProps, mapDispatchToProps)(ModalLogin)
