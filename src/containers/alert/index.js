import React,{ useState,useEffect } from 'react'

import { connect, useDispatch, useSelector } from 'react-redux'

import PropTypes from  'prop-types'
import { Alert } from 'reactstrap';
import styled, { css } from 'styled-components'

const AlertBox = styled('div')`
    display: block;
    width: 100%;
    height: 15rem;
`

const AlertStyled = styled(Alert)`
    //alert neutral
    display: block;
    color: #004085;
    height: 2rem;
    background-color: #cce5ff;
    border-color: #b8daff;

    //override
    ${p => p.type=='info' && css`
        color: #004085;
        background-color: #cce5ff;
        border-color: #b8daff;
    `}

    ${p => p.type=='success' && css`
        color: #004085;
        background-color: #cce5ff;
        border-color: #b8daff;
    `}

    ${p => p.type=='error' && css`
        color: #004085;
        background-color: #cce5ff;
        border-color: #b8daff;
    `}

    ${p => p.type=='warning' && css`
        color: #004085;
        background-color: #cce5ff;
        border-color: #b8daff;
    `}
`

const AlertCustom = props => {

    const alertList = useSelector(state => state.alert.alertList)
    const alertListLenght = useSelector(state => state.alert.alertList.length)
        
    const dispatchAlertClose = useDispatch()

    console.log('!!!!!',alertListLenght )

    const alertClose = id => dispatchAlertClose({ type: "ALERT_CLOSE", payload:{ id } })

    useEffect(() => {
        console.log('xxxxx')
        return () => {
            
        };
    },[alertList.length])

console.log(props.alertList)
    return (
        <AlertBox>
        <h1>quantos::{alertList.lenght}</h1>
            {
                alertList.map(alert =>{
                    if(alert.isOpen){
                        return (
                            <AlertStyled
                                style={{display: 'block'}}
                                key={alert.id}
                                toogle={()=>alertClose(alert.id)} 
                                id={alert.id}
                                timelife={alert.timeLife} 
                                timeclose={alert.timeClose}>
                                    <div className='alert-title'> {alert.title || 'lllll'} </div>
                                    <div className='alert-message'> {alert.message} </div>
                            </AlertStyled>
                        )
                    }
                }).filter(v => v!= undefined)
            }
        </AlertBox>
    )
}

export default (AlertCustom)
  



