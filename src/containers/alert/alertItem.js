import React,{ useState,useEffect } from 'react'

import { UncontrolledAlert } from 'reactstrap';
import styled, { css } from 'styled-components'

const AlertStyled = styled(UncontrolledAlert)`
    ////alert neutral
    display: block;
    color: #004085;
    height: 2rem;
    background-color: #cce5ff;
    border-color: #b8daff;

    //override
    ${p => p.type=='info' && css`
        color: #004085;
        background-color: #cce5ff;
        border-color: #b8daff;
    `}

    ${p => p.type=='success' && css`
        color: #004085;
        background-color: #cce5ff;
        border-color: #b8daff;
    `}

    ${p => p.type=='error' && css`
        color: #004085;
        background-color: #cce5ff;
        border-color: #b8daff;
    `}

    ${p => p.type=='warning' && css`
        color: #004085;
        background-color: #cce5ff;
        border-color: #b8daff;
    `}
`

const AlertItem = props => {

    const { timeLife, timeClose, title, message } = props

    const [visible, setVisible] = useState(true);
    const onDismiss = () => setVisible(false);
    
    const id = Math.random()

    return (
        <AlertStyled
            key={id}
            isOpen={visible} 
            toggle={onDismiss} 
            id={id}
            timelife={timeLife} 
            timeclose={timeClose}>
                <div className='alert-title'> {title || 'lllll'} </div>
                <div className='alert-message'> {message} </div>
        </AlertStyled>
    )
}

export default AlertItem