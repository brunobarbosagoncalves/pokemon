import React,{ useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

const AlertCustom = props => {

    const dispatch = useDispatch()

    const usernameSel = useSelector(state => state.user.username)
    const emailSel = useSelector(state => state.user.email)

    const [username, setUsername] = useState(usernameSel)
    const [email, setEmail] = useState(emailSel)
        
    const updateName = () => dispatch({ type: "USER_UPDATE_NAME", payload:{ username } })

    const loadInit = () => { /* */}

    const clear = () => { /* */}

    useEffect(() => {
        loadInit()
        return () => {
            clear
        };
    },[username, email])

    return (
        <React.Fragment>
            <p>${`Username:: ${username}, email: ${email}`}</p>
            <input type='text' value={username} onChange={(e)=>setUsername(e.target.value)} />
            <button type='button' onClick={updateName}>Update username</button>
        </React.Fragment>
    )
}

export default AlertCustom
  



