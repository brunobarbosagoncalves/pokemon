
import React, { useState } from 'react';

import PropTypes from 'prop-types';
import styled from 'styled-components'
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';

const DropdownItemStyled =  styled(DropdownItem)
.attrs({})`

`

const DropdownItemCustom = props => {
    const { 
        title,
        params={}
    } = props

    return(
        <DropdownItemStyled {...params} {...props}>
            {title}
        </DropdownItemStyled>           
    )
}

DropdownItemCustom.propTypes ={
    itens: PropTypes.array.isRequired,
    params: PropTypes.object,
}

export default DropdownItemCustom