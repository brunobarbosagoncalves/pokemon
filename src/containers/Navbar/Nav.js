import React from 'react'

import styled from 'styled-components'
import { Navbar } from 'reactstrap';
import PropTypes from 'prop-types';
import COLORS from 'constants/colors'

const NavStyled = styled(Navbar)
.attrs({})`
  background-color: ${COLORS.BLUE} !important;
`

const NavCustom = props => {
  const { children } = props
  return(
    <div className='row'>
      <NavStyled className='w-100' color="light" light expand="md">
          {children}
      </NavStyled>
    </div>
  )
}

NavCustom.propTypes ={
  children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
  ]).isRequired,
}

export default NavCustom