
import React from 'react';
import PropTypes from 'prop-types';
import styled,{ css } from 'styled-components'
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu
} from 'reactstrap';

const UncontrolledDropdowStyled =  styled(UncontrolledDropdown)
.attrs({})`

`

const DropdownToggleStyled = styled(DropdownToggle).attrs({})`
    color: black;

    //override
    ${p => p.color && css`
        color: ${p.color} !important;
    `}

    ${p => p.right && css`
    text-align: right !important;
`}
`

const UncontrolledDropdownCustom = props => {
    const { 
        title, 
        direction = 'right'
    } = props

    return(
        <UncontrolledDropdowStyled nav inNavbar {...props}>
            <DropdownToggleStyled className='text-truncate' nav caret {...props}>
                {title}
            </DropdownToggleStyled>
            <DropdownMenu direction={direction}>
                <React.Fragment>
                    {props.children}
                </React.Fragment>
            </DropdownMenu>
        </UncontrolledDropdowStyled>    
    )
}

UncontrolledDropdownCustom.propTypes ={
    title: PropTypes.string.isRequired,
    direction: PropTypes.string
}


export default UncontrolledDropdownCustom