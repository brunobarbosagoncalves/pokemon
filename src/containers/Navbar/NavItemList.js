import React, { useState } from 'react';

import PropTypes from 'prop-types';
import styled, { css } from 'styled-components'
import { Nav, NavbarToggler, Collapse } from 'reactstrap';

const NavItensStyled =  styled(Nav)
.attrs({})`

    //override
    ${p => p.color && css`
        color: ${p.color};
    `}
`

const NavItensCustom = props => {

    const { 
        children, 
        className='mr-auto',
        params={},
    } = props

    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);


    return(
        <React.Fragment>
            <NavbarToggler onClick={toggle} />
            <Collapse isOpen={isOpen} navbar>
                <NavItensStyled navbar className={className} {...params} {...props} >
                    <React.Fragment>
                        {children}
                    </React.Fragment>           
                </NavItensStyled>
            </Collapse>
        </React.Fragment>
    )
}

NavItensCustom.propTypes ={
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired,
    params: PropTypes.object,
    className: PropTypes.string,
}

export default NavItensCustom