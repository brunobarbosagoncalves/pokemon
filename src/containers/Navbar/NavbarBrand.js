import React from 'react'

import styled, { css } from 'styled-components'
import { NavbarBrand } from 'reactstrap'

import COLORS from 'constants/colors'

const NavbarBrandStyled = styled(NavbarBrand)
.attrs({})`
    backgrouns-color: ${COLORS.BLUE};

    //override
    ${p => p.bgcolor && css`
        background-color: ${p.bgcolor || 'white'};
    `}
`


const NavbarBrandCustom = props => {
    return(
        <NavbarBrandStyled {...props}>
            {props.children}
        </NavbarBrandStyled>
    )
}

export default NavbarBrandCustom