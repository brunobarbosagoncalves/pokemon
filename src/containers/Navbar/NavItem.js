
import React, { useState } from 'react';

import PropTypes from 'prop-types';
import styled from 'styled-components'
import {
  NavItem
} from 'reactstrap';

const NavItemStyled =  styled(NavItem)
.attrs({})`

`

const NavItemCustom = props => {
    const { 
        title,
        params={},
        children
    } = props

    return(
        <NavItemStyled {...params} {...props}>
            {children || title}
        </NavItemStyled>           
    )
}

NavItemCustom.propTypes ={
    params: PropTypes.object,
}

export default NavItemCustom