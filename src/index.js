import React, { Suspense, lazy, Fragment } from 'react'
import ReactDOM from 'react-dom'

/* CSS */
import 'bootstrap/dist/css/bootstrap.min.css'
import './assets/styles/custom.css'

import * as serviceWorker from './serviceWorker'

import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'
import { Provider } from 'react-redux'
import Store from 'stores'

/* containers */

/* Pages */
import Splash from 'pages/splash'
import Home from 'pages/public/pages/home'
import Details from 'pages/public/pages/details'
import Login from 'pages/public/login'

/* Toasts and Alerts */
import ToastContainer from 'components/Feedback'
import 'react-toastify/dist/ReactToastify.css'

//import Modals from "containers/modals"

const PrivateComponent = () => (
  <React.Fragment>
    <h1>PRIVATE</h1>
    <p>Before show page check permitions in TOKEN | JWT</p>
  </React.Fragment>
)

const ValidateAccess = () => true

const PrivateRoute = ({ component: Component, ...rest }) =>
  ValidateAccess() ? (
    <Route
      {...rest}
      render={props => (
        <React.Fragment>
          <Component {...props} />
        </React.Fragment>
      )}
    />
  ) : (
    <Redirect to={'/login'} />
  )

ReactDOM.render(
  <Provider store={Store}>
    <Splash>
      <React.Fragment>
        <BrowserRouter>
          <Switch>
            {/* Public */}
            <Route exact path="/" component={Home} />
            <Route exact path="/details/:id/:name" component={Details} />

            {/* Secure */}
            <PrivateRoute exact path="/private" component={PrivateComponent} />

            {/* 404 */}
            <Route component={() => <h1>No Match Route</h1>} />
          </Switch>
        </BrowserRouter>
        {/* import all modals */}
        {/*<Modals />*/}

        <ToastContainer
          enableMultiContainer
          containerId={'ALERT'}
          toastClassName={'feedback-alert'}
        />
        <ToastContainer
          enableMultiContainer
          containerId={'TOAST'}
          toastClassName={'feedback-toast'}
        />
      </React.Fragment>
    </Splash>
  </Provider>,
  document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
