/**
 * Traducoes do sistema
 */

import intervalPlural from 'i18next-intervalplural-postprocessor';
import LanguageDetector from 'i18next-browser-languagedetector';
import moment from 'moment'

import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';
import Backend from 'i18next-xhr-backend';

//Resources
import resourcesList from './resources'

export const resources = {
    ...resourcesList
}

/* environment */
const DEBUG_I18N = process.env.DEBUG_I18N || false
const NODE_ENV = process.env.NODE_ENV == 'development' || false

/**
 * Examples
 * key: "The current date is {{date, MM/DD/YYYY}}",
 * key2: "{{text, uppercase}} just uppercased"
 */
const interpolation = {
    format: function(value, format, lng) {
        console.log('value::', value, ', format::', format, ', lng::', lng)
        if (format === 'uppercase') return value.toUpperCase();
        if(value instanceof Date) return moment(value).format(format);
        return value;
    }
}

i18next
    .use(intervalPlural)
    .use(Backend)
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        lng: 'ptbr',
        debug: DEBUG_I18N || NODE_ENV || false,
        resources,
        interpolation,
        useSuspense:true,
        postProcess:true,

    });

i18next.on('languageChanged', function(lng) {
    moment.locale(lng);
});


export default i18next


 