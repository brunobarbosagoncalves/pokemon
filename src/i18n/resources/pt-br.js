export default {
    translation: {
        language:'idioma',
        car:`carro`,
        friend:`amigo`,
        
        friend_plural:`amigos`,
        client_name:'nome do cliente',
        
        friend_male:`amigo homem`,
        friend_female:`amiga mulher`,
        friend_male_plural:`{{count}} amigos homens`,
        friend_female_plural:`{{count}} amigas mulheres`,

        friend_interval:`
            (0){Nenhum amigo};
            (1-2){Alguns amigos};
            (3-inf){Muitos amigos}`,
            
        apresentation:`Ola meu nome é {{name}}, e eu tenho {{age}} anos`

    } 
}
