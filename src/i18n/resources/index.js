import ptbr from './pt-br'
import en from './en'

export default {
    ptbr,
    en
}