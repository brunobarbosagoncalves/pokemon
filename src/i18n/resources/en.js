export default {
    translation: {
        language:'language',
        car:`car`,
        friend:`friend`,
        
        friend_plural:`friends`,
        client_name:'name client',
        
        friend_male:`frien man`,
        friend_female:`friend woman`,
        friend_male_plural:`{{count}} friends man`,
        friend_female_plural:`{{count}} friends woman`,

        friend_interval:`
            (0){None friend};
            (1-2){Some friends};
            (3-inf){Very friends}`,
            
        apresentation:`Hello my name is {{name}}, and i have {{age}} years old`
    } 
}
