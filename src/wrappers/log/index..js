const DEBUG_LOG = process.env.DEBUG_LOG || false

const PATTERN_LOG = {
  origin: "", // external/internal
  bruteData: "", //String
  httpCode: "000",
  httpText: "",
  headers: {},
  data: {},
  operationLocalId: "",
  operationGlobalId: "",
  operationStartAt: "", //ISO Date
  operationEndAt: "", //ISO Date
  operationStartTsAt: "", // Number
  operationEndTsAt: "", // Number
  ownerId: "" // User Id
}

// Type logs httpRequest(axios)
export const logData = (params, callback = false, debug = false) => {
  const log = { ...PATTERN_LOG, ...params }
  //Because console.log is sync
  if (debug || DEBUG_LOG) {
    new Promise(resolve => {
      console.log(log)
      resolve()
    })
  }

  // Valid callback
  if (callback) {
    callback(log)
  }
}

// Type logs serverRequest(express)
export const logServerRequest = (requisition, callback = false, debug = false) => {
  const log = { ...PATTERN_LOG /*, ...params*/ }
  //Because console.log is sync
  if (debug) {
    new Promise(resolve => {
      console.log(log)
      resolve()
    })
  }

  // Valid callback
  if (callback) {
    callback(log)
  }
}

const getFieldsFromRequestServer = req => {
  const { request, response, ...reqRest } = req
  const PATTERN_LOG = {
    origin: "internal",
    bruteData: JSON.stringify(reqRest),
    httpCode: "000",
    httpText: "",
    headers: {},
    data: {},
    operationLocalId: "",
    operationGlobalId: "",
    operationStartAt: "", //ISO Date
    operationEndAt: "", //ISO Date
    operationStartTsAt: "", // Number
    operationEndTsAt: "", // Number
    ownerId: "" // User Id
  }
}
