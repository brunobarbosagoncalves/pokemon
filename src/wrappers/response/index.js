class Response {

    constructor(){
        this.hasError=false
        this.data={}
    }

    setHasError(p){
        if(typeof p == 'boolean'){
            this.hasError = p
            return
        }   
        return new Error('setHasError not has value boolean(true || false')
    }

    setData(data){
        if(typeof data != 'undefined' && typeof data != 'null'){
            this.data= data
            return
        }
    }

    getData(){
        return ({
            hasError: this.hasError,
            data: this.data
        })
    }

}

export default Response = new Response()