/**
 * ------------------------------------------------------
 * Structure folder and files and scripts to proccess work
 * ------------------------------------------------------
 *
 * SCRIPTS
 *  ...
 *  "scripts": {
 *    "kill-server": "packageServer commandKillAllServer",
 *    "build": "packageBuild folderOrigin folderDestiny ",
 *    "prod": "set NODE_ENV=production && otherCommand",
 *    "dev": "set NODE_ENV=developer && otherCommand",
 *    "test": "set NODE_ENV=test && otherCommand"
 *  }
 *  ...
 *
 * FOLDERS
 * rootProject/dotenv (folder)
 * rootProject/dotenv/.dotenv.example (file)
 * rootProject/dotenv/.dotenv.development (file)
 * rootProject/dotenv/.dotenv.production (file)
 * rootProject/dotenv/.dotenv.test (file)
 * rootProject/dotenv/.dotenv.rules (file)
 *    SERVER_PORT=1234
 *    DB_NAME=mydb
 *    DB_PASSWORD=123456789
 *    DB_URL=www.where.com.br
 *
 * .dotenv.rules
 *    +Contain keys required in all files .dotenv.{NODE_ENV}
 * .dotenv.example
 *    +File with all keys and values required to work
 * .dotenv.[development,production,test]
 *    +Files with rules and variables on environments
 */

import dotenv from "dotenv"

import fs from "fs"
import path from "path"

const NODE_ENV = process.env.NODE_ENV
const NODE_ENV_OPTIONS = ["production", "development", "test"]
const BAR = path.sep
const PATH_FILE_ENVIRONMENT = `.${BAR}dotenv${BAR}.dotenv`
const PATH_FILE_ENVIRONMENT_FULL = `${PATH_FILE_ENVIRONMENT}.${NODE_ENV}`.trim()
const PATH_FILE_ENVIRONMENT_RULES = `${PATH_FILE_ENVIRONMENT}.rules`.trim()
let env_full = ""
let env_rules = ""

export default () => {
  // Check if NODE_ENV value is valid
  if (!NODE_ENV_OPTIONS.includes(NODE_ENV.trim())) {
    throw `Environment NODE_ENV require one value: production, development or test, but found ${NODE_ENV}" in file`
  }

  // Check if file .dotenv.{NODE_ENV} exist
  fs.exists(PATH_FILE_ENVIRONMENT_FULL, isExist => {
    if (!isExist) {
      throw `Environment file "${PATH_FILE_ENVIRONMENT_FULL}" not exist`
    }
  })

  env_full = GetFilesKeys(PATH_FILE_ENVIRONMENT_FULL)
  env_rules = GetFilesKeys(PATH_FILE_ENVIRONMENT_RULES)

  // check if all keys in .dotenv.rules exists in .dotenv.{NODE_ENV} file
  if (env_full !== env_rules) {
    throw `Environment file "${PATH_FILE_ENVIRONMENT_FULL}" and "${PATH_FILE_ENVIRONMENT_RULES}" dont have same keys`
  }

  // import and load file
  dotenv.config({ path: PATH_FILE_ENVIRONMENT_FULL })
}

const GetFilesKeys = pathFile => {
  const keys = []
  fs.readFileSync(pathFile)
    .toString()
    .split("\n")
    .forEach(line => {
      if (line != undefined && line.length) {
        //Clean and set var
        const obj = line.replace(/[\n\r]/g, "").split("=")
        keys.push(obj[0])
      }
    })

  return keys.sort().join(",")
}
