import axios from "axios"

const DEBUG_REQUEST = process.env.DEBUG_REQUEST || false

/**
 * @param {String} method
 * @param {String} url
 * @param {Object} options : { data:{...}, headers:{...} }
 * @param {Function} callback : data => { ...sendData }
 */
export const Call = async (method, url, options, callback = false) =>
  axios({ method, url, options })
    .then(result => {
      //const { data, status, statusText, headers, config, request } = result
      CallDebug(result.data)

      if(callback){
        callback(result.data)
      }

      return Promise.resolve({ data: result.data, hasError: false })
    })
    .catch(error => {
      const { request, response, config, ...result } = error

      CallDebug(result)

      if(callback){
        callback(result)
      }

      return Promise.resolve({ data: result, hasError: true })
    })

export const CallDebug = data => {
  // check if debug enabled
  if (!DEBUG_REQUEST) {
    return false
  }

  return new Promise((resolve, reject) => {
    try {
      resolve()
      console.log({ requestDebug: true, data })
    } catch (error) {}
  })
}

export default Call
