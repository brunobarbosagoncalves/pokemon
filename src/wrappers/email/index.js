import nodemailer from "nodemailer"

const EMAIL_USER = process.env.EMAIL_USER || "123brunoteste123"
const EMAIL_PASSWORD = process.env.EMAIL_PASSWORD || "123senha123"
const EMAIL_HOST = process.env.EMAIL_HOST || "smtp.gmail.com"
const EMAIL_PORT = process.env.EMAIL_PORT || 587
const EMAIL_SECURE = process.env.EMAIL_SECURE || false
const EMAIL_TLS_REJECT_UNAUTHORIZED = process.env.EMAIL_TLS_REJECT_UNAUTHORIZED || false

export const getTransporterOptionsDefault = _ => ({
  host: EMAIL_HOST,
  port: EMAIL_PORT,
  secure: EMAIL_SECURE,
  auth: {
    user: EMAIL_USER,
    pass: EMAIL_PASSWORD
  },
  tls: {
    // do not fail on invalid certs
    rejectUnauthorized: EMAIL_TLS_REJECT_UNAUTHORIZED
  }
})

export const getMailOptionsDefault = _ => ({
  attachments: [],
  headers: {},
  from: "123brunoteste123@gmail.com",
  to: "",
  cc: "",
  bcc: "",
  subject: "teste XXX",
  text: "Email test " + new Date(),
  html: "",
  encoding: "utf8"
})

/**
 * @param {Object} transporterOptions : {  }
 * @param {Object} mailOptions : {  }
 * @see transporterOptions : https://nodemailer.com/smtp/
 * @see mailOptions : https://nodemailer.com/message/
 */
export default function sendEmail(transporterOptions, mailOptions) {
  const transportConfig = {
    ...getTransporterOptionsDefault(),
    ...transporterOptions
  }
  const mailConfig = {
    ...getMailOptionsDefault(),
    ...mailOptions
  }

  const transporter = nodemailer.createTransport(transportConfig)

  return new Promise((resolve, reject) => {
    transporter.sendMail(mailConfig, (err, info) => {
      if (err) {
        reject({ hasError: true, data: err })
      }

      resolve({ hasError: false, data: info })
    })
  })
}
